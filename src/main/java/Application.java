import utils.GraphBuilder;
import model.SimpleEdge;
import model.TreeGraph;
import model.TreeNode;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/***
 * 工具程序入口
 * 包含了若干历史案件程序
 *
 * @author xuda
 */
public class Application {
    public static void main(String[] args) throws IOException {
        GraphBuilder graphBuilder = new GraphBuilder();
//        List<TreeGraph> treeGraphList = graphBuilder.buildTreeGraphFromCSV_sdkj_user();//正常GraphBuilder的build方法只返回一个图
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_q_account();

        //找根&建树
//        treeGraph.detectRoot();
//        treeGraph.selfBuildTree(0,true, 0);
//        for(TreeGraph g : treeGraphList){
//            g.detectRoot();
//            g.selfBuildTree(0,true, 0);//第0个根构建
//        }

        //计算&打印&保存 每个子树分支节点数
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        treeGraph.printTraversal();
//        treeGraph.saveBranchInfo(treeGraph.getName() + "_branchInfo.txt");
//        for(TreeGraph g : treeGraphList){
//            g.traverseTree(g.getRoots().get(0));
//            g.saveBranchInfo(g.getName() + "_branchInfo.txt");
//            g.printTraversal();
//        }

//        //打印&保存 树图信息（json）
//        treeGraph.printTreeGraphJson(0);
//        treeGraph.saveTreeGraphJson(0,treeGraph.getName() + ".json");
//        for(TreeGraph g : treeGraphList){
//            g.printTreeGraphJson(0);
//            g.saveTreeGraphJson(0, g.getName() + ".json");
//        }
//
//        //打印&保存 树图层序信息（txt）
//        treeGraph.printLevelInfo();
//        treeGraph.saveLevelInfo(treeGraph.getName() + "_levelInfo.txt");
//        for(TreeGraph g : treeGraphList){
//            g.printLevelInfo();
//            g.saveLevelInfo(g.getName() + "_levelInfo.txt");
//        }

        /***
         * 6月4日新案件 YS.csv YS-FULL.csv
         * 91,94,1227,1239,2080,2751,3996
         */
//        TreeGraph treeGraphYS = graphBuilder.buildTreeGraphFromCSV_YS_FULL();
//        treeGraphYS.detectRoot("0");
//        treeGraphYS.setName("YS-0-full");
//        long startTime = System.currentTimeMillis();
//        treeGraphYS.selfBuildTree(0,true, 0);
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraphYS.traverseTree(treeGraphYS.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
////        treeGraphYS.saveBranchInfo(treeGraphYS.getName() + "_branchInfo.txt");
////        treeGraphYS.saveTreeGraphJson(0,treeGraphYS.getName() + ".json");
////        treeGraphYS.saveLevelInfo(treeGraphYS.getName() + "_levelInfo.txt");
//        treeGraphYS.getPathFromRoot(817777);

        /***
         * 6月7日YS案件新需求 慧文
         */
//        TreeGraph treeGraphYS = graphBuilder.buildTreeGraphFromCSV_YS_FULL();
//        treeGraphYS.detectRoot("0");
//        treeGraphYS.setName("YS-0-full");
//        long startTime = System.currentTimeMillis();
//        treeGraphYS.selfBuildTree(0,true, 0);
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraphYS.traverseTree(treeGraphYS.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        Map<Integer, UserName> userNameMap = graphBuilder.getUerNamefromCSV();
//        Map<Integer, UserInfoMarch> userInfoMarchMap = graphBuilder.getUserInfoMarchfromCSV();
//
//        List<Integer> l = new ArrayList<Integer>();
//        l.add(23);
////        l.add(91);
////        l.add(94);
////        l.add(1227);
////        l.add(1239);
////        l.add(2080);
////        l.add(2751);
////        l.add(3996);
//        for(Integer i: l){
//            Map<Integer, ReportItem> reportMap = new HashMap<Integer, ReportItem>();
//            treeGraphYS.getSubTree(i, true, 0, userNameMap, userInfoMarchMap, reportMap);
//            // 遍历report
//            System.out.println( "starting from " + i + " report item size: " + reportMap.size());
////            for(Map.Entry<Integer, ReportItem> entry : reportMap.entrySet()) {
////                Integer id = entry.getKey();
////                ReportItem item = entry.getValue();
////                System.out.println(item.toString());
////            }
//            // 写入 excel
//            graphBuilder.exportToExcel("YS-"+ i + ".xlsx", reportMap);
//        }

        /***
         * 6月9日，兆帅案件 推荐人
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_zhaoshuai_tuijian();
//        treeGraph.setRoot("2");
//        treeGraph.setName("Zhaoshuai-TuiJian");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 15);
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        treeGraph.saveBranchInfo(treeGraph.getName() + "_branchInfo.txt");
//        treeGraph.saveTreeGraphJson(0,treeGraph.getName() + ".json");
//        treeGraph.saveLevelInfo(treeGraph.getName() + "_levelInfo.txt");

        /***
         * 6月9日，兆帅案件 安置人
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_zhaoshuai_anzhi();
//        treeGraph.setRoot("2");
//        treeGraph.setName("Zhaoshuai-AnZhi");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 15);
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        treeGraph.saveBranchInfo(treeGraph.getName() + "_branchInfo.txt");
//        treeGraph.saveTreeGraphJson(0,treeGraph.getName() + ".json");
//        treeGraph.saveLevelInfo(treeGraph.getName() + "_levelInfo.txt");

        /***
         * 6月9日，兆帅案件，重新做q_account的图，
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_q_account_full();
//        treeGraph.setRoot("0");
//        treeGraph.setName("q_account_full");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        treeGraph.saveBranchInfo(treeGraph.getName() + "_branchInfo.txt");
//        treeGraph.saveTreeGraphJson(0,treeGraph.getName() + ".json");
//        treeGraph.saveLevelInfo(treeGraph.getName() + "_levelInfo.txt");

        /***
         * 6月10日，张luo，YS案件最新进展
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general("用户信息表","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/用户信息表.csv");
//        treeGraph.setRoot("0");
//        treeGraph.setName("YS-UPDATE");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//
//        List<Integer> l = new ArrayList<Integer>();
//
//        l.add(91);
//        l.add(94);
//        l.add(1227);
//        l.add(1239);
//        l.add(2751);
//        l.add(2080);
//        l.add(3996);
//        l.add(20297);
//        l.add(29626);
//        l.add(26932);
//        l.add(24415);
//        for(Integer i: l){
//            TreeNode currentNode = treeGraph.getSubTree(i, true, 0);
//            List<NodeProperties> excelList = new ArrayList<>();
//            treeGraph.buildExcelOutput(currentNode, true, 0, excelList);
//            graphBuilder.exportToExcel("YS-UPDATE-"+ i + ".xlsx", "YS-UPDATE-"+ i, excelList);
//        }

        /***
         * 7月15日，慧文，
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general2("用户信息表","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users.csv");
//        treeGraph.setRoot("0");
//        treeGraph.setName("users");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//
//        Map<Integer, Integer> levelMap = new HashMap<>();
//        Map<Integer, Integer> nowLevelMap = new HashMap<>();
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users.csv"));
//        String titleLine = reader.readLine();//第一行信息，为标题信息
//        String columnName[] = titleLine.split("\",");
//        int columnCount = columnName.length;//第一行有多少个元素，
//        String line = null;
//        while ((line = reader.readLine()) != null) {
//            String item[] = line.split("\",", -1);//不忽略空值
//            if (item.length == columnCount) {
//                Integer id = Integer.parseInt(item[0].replaceAll("\"", ""));
//                Integer level = Integer.parseInt(item[6].replaceAll("\"", ""));
//                Integer nowLevel = Integer.parseInt(item[7].replaceAll("\"", ""));
//                levelMap.put(id,level);
//                nowLevelMap.put(id,nowLevel);
//            }
//            else{
//                System.out.println("invalid line: " + line);
//                System.out.println(" 本行列数：" + item.length);
//            }
//        }
//        reader.close();
//        System.out.println(" map size: " + levelMap.size() + " " + nowLevelMap.size());
//
//        List<Integer> l = new ArrayList<Integer>();
//        l.add(314);
//        l.add(327);
//        l.add(1192);
//        l.add(1190);
//        l.add(1657);
//        l.add(1772);
//        l.add(15986);
//        l.add(74);
//        l.add(91);
//        l.add(94);
//        l.add(188);
//        l.add(26932);
//        l.add(3996);
//        l.add(20676);
//        l.add(29626);
//        l.add(14607);
//        l.add(14990);
//        l.add(15015);
//        l.add(15221);
//        l.add(20297);
//        l.add(1227);
//        l.add(1239);
//        l.add(1241);
//        l.add(2080);
//        l.add(2751);
//
//        for(Integer i: l){
//            TreeNode currentNode = treeGraph.getSubTree(i, true, 0);
//            List<String> descendants = new ArrayList<>();
//            treeGraph.getAllDescendants(currentNode,true, 0, descendants);
////            System.out.println("应该相同：" + descendants.get(0) + " " + i);
//            int level_0 = 0;
//            int level_1 = 0;
//            int level_2 = 0;
//            int level_3 = 0;
//            int level_4 = 0;
//            int now_level_0 = 0;
//            int now_level_1 = 0;
//            int now_level_2 = 0;
//            int now_level_3 = 0;
//            int now_level_4 = 0;
//            for(int j = 1; j < descendants.size(); j ++){
//                int currentId = Integer.parseInt(descendants.get(j));
//                if(levelMap.get(currentId) == 0){
//                    level_0 ++;
//                }else if(levelMap.get(currentId) == 1){
//                    level_1 ++;
//                }else if(levelMap.get(currentId) == 2){
//                    level_2 ++;
//                }else if(levelMap.get(currentId) == 3){
//                    level_3 ++;
//                }else if(levelMap.get(currentId) == 4){
//                    level_4 ++;
//                }
//                if(nowLevelMap.get(currentId) == 0){
//                    now_level_0 ++;
//                }else if(nowLevelMap.get(currentId) == 1){
//                    now_level_1 ++;
//                }else if(nowLevelMap.get(currentId) == 2){
//                    now_level_2 ++;
//                }else if(nowLevelMap.get(currentId) == 3){
//                    now_level_3 ++;
//                }else if(nowLevelMap.get(currentId) == 4){
//                    now_level_4 ++;
//                }
//            }
//            System.out.println(i + ", " + (currentNode.getBranchCount()-1) + ", " + currentNode.getBranchHeight() + ", " + level_0 + ", " + level_1 + ", " + level_2 + ", " + level_3 + ", " + level_4 + ", " + now_level_0 + ", " + now_level_1 + ", " + now_level_2 + ", " + now_level_3 + ", " + now_level_4);
//        }

        /****
         * 7月27日 慧文
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general2("用户信息表","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/q_account 7-27.csv");
//        treeGraph.setRoot("0");
//        treeGraph.setName("users");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//
//        BufferedWriter txtWriter = null;
//        try {
//            txtWriter = new BufferedWriter(new FileWriter("慧文-7月27日.txt"));
//            for(Map.Entry<Integer, TreeNode> entry : treeGraph.getTraversal().entrySet()) {
//                Integer id = entry.getKey();
//                TreeNode node = entry.getValue();
//                String line = id + ";" + node.getParentId() + ";" + treeGraph.getPathFromRoot(id) + ";" + node.getLevel() + ";" + node.getBranchHeight() + ";" + (node.getBranchCount() - 1) + "\n";
//                txtWriter.write(line);
//            }
//            txtWriter.flush();
//            txtWriter.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        /***
         * 8月3日 杨亮
         * 3M案件，和之前案件最大区别是原始数据没有id，只有hash，需要使用id-label map，自定义做对应
         */
//        String upHtml = "<!DOCTYPE html>\n" +
//                "<html lang=\"en\">\n" +
//                "<head>\n" +
//                "    <meta charset=\"UTF-8\" />\n" +
//                "    <title>Tutorial Demo</title>\n" +
//                "    <style>\n" +
//                "        /* 提示框的样式 */\n" +
//                "        .g6-tooltip {\n" +
//                "            border: 1px solid #e2e2e2;\n" +
//                "            border-radius: 4px;\n" +
//                "            font-size: 12px;\n" +
//                "            color: #545454;\n" +
//                "            background-color: rgba(255, 255, 255, 0.9);\n" +
//                "            padding: 10px 8px;\n" +
//                "            box-shadow: rgb(174, 174, 174) 0px 0px 10px;\n" +
//                "        }\n" +
//                "        #mountNode {\n" +
//                "            background-color: white;\n" +
//                "            width: 2000px;\n" +
//                "            height: 1600px;\n" +
//                "        }\n" +
//                "    </style>\n" +
//                "</head>\n" +
//                "<body>\n" +
//                "<div id=\"mountNode\"></div>\n" +
//                "<script src=\"./antv.js\"></script>\n" +
//                "<script>\n" +
//                "    // const localData = // 本地数据粘贴到这\n";
//        String jsonHead = "const smallLocalData = ";
//        String downHtml = "const localData = smallLocalData;\n" +
//                "    const button = document.createElement('button');\n" +
//                "    button.innerHTML = '全部展开/收起';\n" +
//                "    const container = document.getElementById('mountNode');\n" +
//                "    container.appendChild(button);\n" +
//                "    button.addEventListener('click', (e) => {\n" +
//                "        // console.log(\"expand/collapse\")\n" +
//                "        if(localData.collapsed){\n" +
//                "            // let reg = new RegExp(\"true\",\"g\")//g表示全部的\n" +
//                "            setCollapsed(localData, false);\n" +
//                "            localData.collapsed = false;\n" +
//                "        }else{\n" +
//                "            // let reg = new RegExp(\"false\",\"g\")//g表示全部的\n" +
//                "            setCollapsed(localData, true);\n" +
//                "            localData.collapsed = true;\n" +
//                "        }\n" +
//                "        graph.data(localData);\n" +
//                "        graph.render();\n" +
//                "        graph.layout();\n" +
//                "    });\n" +
//                "\n" +
//                "    /***\n" +
//                "     * 递归设置展开/收起属性\n" +
//                "     * @param data\n" +
//                "     * @param isCollapsed\n" +
//                "     */\n" +
//                "    function setCollapsed(data, isCollapsed){\n" +
//                "        // console.log(\"children size: \" + data.children.length);\n" +
//                "        // let children = data.children;\n" +
//                "        if(data.children){\n" +
//                "            data.collapsed = isCollapsed;\n" +
//                "            for (const child in data.children) {\n" +
//                "                setCollapsed(data.children[child], isCollapsed);\n" +
//                "            }\n" +
//                "        }\n" +
//                "    }\n" +
//                "    // 实例化 grid 插件\n" +
//                "    const grid = new G6.Grid();\n" +
//                "    const graph = new G6.TreeGraph({\n" +
//                "        fitView: true,\n" +
//                "        container: 'mountNode',\n" +
//                "        width:2000,\n" +
//                "        height:1600,\n" +
//                "        modes: {\n" +
//                "            default: [\n" +
//                "                {\n" +
//                "                    type: 'collapse-expand',\n" +
//                "                    onChange: function onChange(item, collapsed) {\n" +
//                "                        const data = item.get('model');\n" +
//                "                        data.collapsed = collapsed;\n" +
//                "                        return true;\n" +
//                "                    },\n" +
//                "                },\n" +
//                "                {\n" +
//                "                        type: 'tooltip', // 提示框\n" +
//                "                        offset: 50,\n" +
//                "                        formatText(model) {\n" +
//                "                            // 提示框文本内容\n" +
//                "                            const text = '伞下人数(含自身): ' + model.branchCount + '<br/>' +\n" +
//                "                                '下级层数: ' + model.branchHeight + '<br/>' +\n" +
//                "                                '层级: ' + model.level + '<br/>';\n" +
//                "                            return text;\n" +
//                "                        },\n" +
//                "                },\n" +
//                "                'drag-canvas',\n" +
//                "                'zoom-canvas',\n" +
//                "            ],\n" +
//                "        },\n" +
//                "        defaultNode: {\n" +
//                "            size: 26,\n" +
//                "            anchorPoints: [\n" +
//                "                [0, 0.5],\n" +
//                "                [1, 0.5],\n" +
//                "            ],\n" +
//                "        },\n" +
//                "        defaultEdge: {\n" +
//                "            type: 'cubic-horizontal',\n" +
//                "        },\n" +
//                "        layout: {\n" +
//                "            type: 'dendrogram',\n" +
//                "            direction: 'LR', // H / V / LR / RL / TB / BT\n" +
//                "            nodeSep: 30,\n" +
//                "            rankSep: 100,\n" +
//                "        },\n" +
//                "        nodeStateStyles: {\n" +
//                "            // 鼠标 hover 上节点，即 hover 状态为 true 时的样式\n" +
//                "            hover: {\n" +
//                "                fill: 'lightsteelblue',\n" +
//                "            },\n" +
//                "            // 鼠标点击节点，即 click 状态为 true 时的样式\n" +
//                "            click: {\n" +
//                "                // stroke: '#0e00ff',\n" +
//                "                lineWidth: 3,\n" +
//                "            },\n" +
//                "        }\n" +
//                "    });\n" +
//                "    graph.node(function (node) {\n" +
//                "        return {\n" +
//                "            label: node.label, //+ \" (\" + node.level+ \")\",\n" +
//                "            labelCfg: {\n" +
//                "                position: node.children && node.children.length > 0 ? 'left' : 'right',\n" +
//                "                offset: 5,\n" +
//                "            },\n" +
//                "            style:{ //leaf & branch 颜色区别\n" +
//                "                fill: node.children && node.children.length > 0 ? '#EFF4FF' : '#B0D7F6',\n" +
//                "                lineWidth: node.collapsed  ? 3 : 1,\n" +
//                "            }\n" +
//                "        };\n" +
//                "    });\n" +
//                "    graph.on('itemcollapsed', (e) => {\n" +
//                "        // console.log(e.item);// 当前被操作的节点 item\n" +
//                "        // console.log(e.collapsed);// 当前操作是收起（`true`）还是展开（`false`）\n" +
//                "        if(e.collapsed){\n" +
//                "            // graph.setItemState(e.item, 'click', true); // 设置当前节点的 click 状态为 true\n" +
//                "            e.item.get('model').style.lineWidth = 3;\n" +
//                "            e.item.refresh();\n" +
//                "        }else{\n" +
//                "            // graph.setItemState(e.item, 'click', false); // 设置当前节点的 click 状态为 false\n" +
//                "            e.item.get('model').style.lineWidth = 1;\n" +
//                "            e.item.refresh();\n" +
//                "        }\n" +
//                "    });\n" +
//                "    // graph.on('node:click', (e) => {\n" +
//                "    //     // 先将所有当前是 click 状态的节点置为非 click 状态\n" +
//                "    //     const clickNodes = graph.findAllByState('node', 'click');\n" +
//                "    //     clickNodes.forEach((cn) => {\n" +
//                "    //         graph.setItemState(cn, 'click', false);\n" +
//                "    //     });\n" +
//                "    //     const nodeItem = e.item; // 获取被点击的节点元素对象\n" +
//                "    //     graph.setItemState(nodeItem, 'click', true); // 设置当前节点的 click 状态为 true\n" +
//                "    // });\n" +
//                "    const main = async () => {\n" +
//                "        const response = await fetch(\n" +
//                "            // 'https://gw.alipayobjects.com/os/basement_prod/6cae02ab-4c29-44b2-b1fd-4005688febcb.json', //远程访问\n" +
//                "            'https://gw.alipayobjects.com/os/antvdemo/assets/data/algorithm-category.json'\n" +
//                "            // './parent-localData.json', //本地访问\n" +
//                "        );\n" +
//                "        const remoteData = await response.json();\n" +
//                "\n" +
//                "        graph.data(localData);\n" +
//                "        graph.render();\n" +
//                "    };\n" +
//                "    main();\n" +
//                "</script>\n" +
//                "</body>\n" +
//                "</html>";
//
//        /*先构造对应表*/
//        Set<String> labelSet = new HashSet<String>();
//        BufferedReader preReader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users-from-zhonghua.txt"));
//        String preLine = null;
//        while ((preLine = preReader.readLine()) != null) {
//            String item[] = preLine.split("\t",-1);
//            if(item.length != 2){
//                System.out.println("something wrong with this line: " + preLine);
//            }else{
//                labelSet.add(item[0]);
//                labelSet.add(item[1]);
//            }
//        }
//        Map<Integer, String> idLabelMap = new HashMap<>();//id-hash 对应表
//        Map<String, Integer> labelIdMap = new HashMap<>();//hash-id 对应表
//        int temp = 0;
//        for(String s: labelSet){
//            idLabelMap.put(temp, s);
//            temp ++;
//        }
//        labelIdMap = idLabelMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getValue(), entry -> entry.getKey()));
////        String tempLabel = idLabelMap.get(43149);
////        System.out.println(tempLabel);
////        int tempId = labelIdMap.get("T9yD14Nj9j7xAB4dbGeiX9h8unkKHxuWwb");//找到根
////        System.out.println(tempId);
//
//        /*用id建图并计算*/
//        TreeGraph treeGraph = new TreeGraph();
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users-from-zhonghua.txt"));
//        String line = null;
//        Set<Integer> idSet = new HashSet<Integer>();
//        while ((line = reader.readLine()) != null) {
//            String item[] = line.split("\t",-1);//不忽略空值
//            if(item.length == 2){
//                Integer id1 = labelIdMap.get(item[0]);
//                Integer id2 = labelIdMap.get(item[1]);
//                idSet.add(id1);
//                idSet.add(id2);
//                treeGraph.getInDegree().put(id1, 0);
//                treeGraph.getOutDegree().put(id1, 0);
//                treeGraph.getInDegree().put(id2, 0);
//                treeGraph.getOutDegree().put(id2, 0);
//
//                int source = id2;
//                int target = id1;
//                SimpleEdge simpleEdge = new SimpleEdge(source, target); // parent_id 指向 id
//                //初始化NodeProperties并赋给SimpleEdge
////                NodeProperties properties = new NodeProperties();//本次计算没有属性信息
////                simpleEdge.setNodeProperties(properties);
//                treeGraph.getEdgeSet().add(simpleEdge);
//            }else{
//                System.out.println("invalid line: " + line);
//                System.out.println(" 本行列数：" + item.length);
//            }
//        }
//        treeGraph.selfComputeInOutDegree();
//        System.out.println("nodes count in file: " + idSet.size());
//        System.out.println("edges count in file: " + treeGraph.getEdgeSet().size());
//        reader.close();
//        treeGraph.setIdLabelMap(idLabelMap);//新方法，设置id-label map
//        treeGraph.setRootWithLabel("43149");//一旦设定了id-label map, 最要用这个方法设定root，而不是简单的setRoot
//        treeGraph.setName("users-zhonghua");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//
//        /*生成目标人物json 和 html文件*/
////        BufferedReader suspectReader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/3M数据0802.csv"));
////        String sLine = suspectReader.readLine();//第一行信息，为标题信息
////        while ((sLine = suspectReader.readLine()) != null) {
////            String item[] = sLine.split(",",-1);
////            if(item.length != 7){
////                System.out.println("something wrong with this line: " + sLine);
////            }else{
////                String json = treeGraph.getTreeNodeJson(labelIdMap.get(item[0]));//拿到json
////                BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("/Users/xuda/Documents/3M/"+item[4]+"-"+item[0]+".html"));
////                String htmlContent = upHtml + jsonHead + json + "\n" + downHtml;
////                jsonWriter.write(htmlContent);
////                jsonWriter.flush();
////                jsonWriter.close();
////            }
////        }

        /***
         * 8月6日 慧文 mor用户信息表
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general("用户信息表","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/mor用户信息表.csv", 0, 10);
//        treeGraph.setRoot("0");
//        treeGraph.setName("mor");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        //生成excel
//        TreeNode currentNode = treeGraph.getRoots().get(0);//默认第一个根
//        List<NodeProperties> excelList = new ArrayList<>();
//        treeGraph.buildExcelOutputMor(currentNode, true, 0, excelList);
//        graphBuilder.exportToExcel("mor.xlsx", "mor", excelList);
//        //生成json
//        String json = treeGraph.getTreeNodeJson(currentNode).replace(",\"label\":null", "").replace(",\"subLevel\":-1", "");//拿到json
//        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("mor.json"));
//        jsonWriter.write(json);
//        jsonWriter.flush();
//        jsonWriter.close();

        /***
         * 8月16日 王璐，慧文 层级关系
         *
         */
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_20210816("用户信息表","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/层级关系（id与resettlementPeople_id存在关联关系）.csv", 0, 7);
////        treeGraph.detectRoot();//找到根的id是2
//        treeGraph.setRoot("2","/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/层级关系（id与resettlementPeople_id存在关联关系）.csv", 0);
//        treeGraph.setName("wagnlu");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//        System.out.println("traverse tree time cost: " + (endTime - startTime) + "ms" );
//        //生成excel
//        TreeNode currentNode = treeGraph.getRoots().get(0);//默认第一个根
//        List<NodeProperties> excelList = new ArrayList<>();
//        treeGraph.buildExcelOutput20210816(currentNode, true, 0, excelList);
//        graphBuilder.exportToExcel("层级关系（id与resettlementPeople_id存在关联关系）.xlsx", "层级关系", excelList,"/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/层级关系（id与resettlementPeople_id存在关联关系）.csv");

        /***
         * 8月26日，慧文 与8月3日案件类似，只有hash 没有id
         */
//        Set<String> labelSet = new HashSet<String>();
//        BufferedReader preReader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users 8-26.txt"));
//        String preLine = null;
//        while ((preLine = preReader.readLine()) != null) {
//            String[] item = preLine.split(" ",-1);
//            if(item.length != 2){
//                System.out.println("something wrong with this line: " + preLine);
//            }else{
//                labelSet.add(item[0]);
//                labelSet.add(item[1]);
//            }
//        }
//        Map<Integer, String> idLabelMap = new HashMap<>();//id-hash 对应表
//        Map<String, Integer> labelIdMap = new HashMap<>();//hash-id 对应表
//        int temp = 0;
//        for(String s: labelSet){
//            idLabelMap.put(temp, s);
//            temp ++;
//        }
//        labelIdMap = idLabelMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getValue(), entry -> entry.getKey()));
////        String tempLabel = idLabelMap.get(43149);
////        System.out.println(tempLabel);
////        int tempId = labelIdMap.get("T9yD14Nj9j7xAB4dbGeiX9h8unkKHxuWwb");//找到根
////        System.out.println(tempId);
//
//        /*用id建图并计算*/
//        TreeGraph treeGraph = new TreeGraph();
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/users 8-26.txt"));
//        String line = null;
//        Set<Integer> idSet = new HashSet<Integer>();
//        while ((line = reader.readLine()) != null) {
//            String[] item = line.split(" ",-1);
//            if(item.length == 2){
//                Integer id1 = labelIdMap.get(item[0]);
//                Integer id2 = labelIdMap.get(item[1]);
//                idSet.add(id1);
//                idSet.add(id2);
//                treeGraph.getInDegree().put(id1, 0);
//                treeGraph.getOutDegree().put(id1, 0);
//                treeGraph.getInDegree().put(id2, 0);
//                treeGraph.getOutDegree().put(id2, 0);
//
//                int source = id2;
//                int target = id1;
//                SimpleEdge simpleEdge = new SimpleEdge(source, target); // parent_id 指向 id
//                //初始化NodeProperties并赋给SimpleEdge
////                NodeProperties properties = new NodeProperties();//本次计算没有属性信息
////                simpleEdge.setNodeProperties(properties);
//                treeGraph.getEdgeSet().add(simpleEdge);
//            }else{
//                System.out.println("invalid line: " + line);
//                System.out.println(" 本行列数：" + item.length);
//            }
//        }
//        treeGraph.selfComputeInOutDegree();
//        System.out.println("nodes count in file: " + idSet.size());
//        System.out.println("edges count in file: " + treeGraph.getEdgeSet().size());
//        reader.close();
////        treeGraph.detectRoot();
//        treeGraph.setIdLabelMap(idLabelMap);//新方法，设置id-label map
//        treeGraph.setRootWithLabel("43149");//一旦设定了id-label map, 最要用这个方法设定root，而不是简单的setRoot
//        treeGraph.setName("users 8-26");
//        long startTime = System.currentTimeMillis();
//        treeGraph.selfBuildTree(0,true, 0);//全图
//        long endTime = System.currentTimeMillis();
//        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
//        startTime = System.currentTimeMillis();
//        treeGraph.traverseTree(treeGraph.getRoots().get(0));
//        endTime = System.currentTimeMillis();
//
//        TreeNode currentNode = treeGraph.getRoots().get(0);//默认第一个根
//        List<NodeProperties> excelList = new ArrayList<>();
//        treeGraph.buildExcelOutput20210826(currentNode, true, 0, excelList);
//        graphBuilder.exportToExcel("慧文9-1.xlsx", "层级关系", excelList,"/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/template 8-26.csv");

        /***
         * 10月18日 丹枫，只有 hash 没有 id
         * 由于数据量比较大调整了jvm启动参数：-XX:+UseG1GC -XX:+UseStringDeduplication -Xms1024m -Xmx10240m -Xss64m
         * first scan time: 33s
         * nodes count in file: 11688531
         * edges count in file: 11688530
         * second scan time: 148s
         * danfeng-10-18 root found: 2441527 root in/out: 0/793
         * build tree time cost: 176s
         * traverse tree time cost: 60s
         *
         *
         */
        List<String> l = new ArrayList<>();
        List<Integer> targets = new ArrayList<>();
        BufferedReader targetReader = new BufferedReader(new FileReader("/Users/xuda/Documents/danfeng-graph-targets-10-18"));
        String targetLine = null;
        while ((targetLine = targetReader.readLine()) != null) {
            l.add(targetLine);
        }
        targetReader.close();
//        for (String item : l) {
//            System.out.println(item);
//        }
//        l.add("rDw9zCpUeVMHb7h2yUa5XokMDu9ggntKp9");
//        l.add("rfpFKPmAT38SBVVxzGMDGcW7qSxqvXSH8");
//        l.add("rKocMS9a96LBcz5tgvaG7zYK6TUbXy4Lcv");
//        l.add("rL1srjDb2VT3RUkrSiRqxxPWaJj15PjqVS");
//        l.add("raUhQiS8pegTKE3NJV4ozR14aT6ZWYBD1V");
//        l.add("rMN4b8z3zZBd3qzKZfF3k7WsD5wegRc7wD");
//        l.add("raJs1rUq2oyRU9QmzFfnb7yQ7eWaHPWqsB");
//        l.add("rUw8HmcEtDjGEAPF57j8ZyxYvPYDMMQWZx");
//        l.add("raXeUYzZYfeMLW5vuQmVdRDQBigeLakprb");
//        l.add("rHVmkv655XV5EBnzrYgLkr2VVSSajQDhK3");
//        l.add("rEtsGFNhVW9ShEZk8CJnVxCgGE2cqLWJPg");
//        l.add("rUPH4nDyE5pemUbFTd33d8iTYWz5CBUdfv");
//        l.add("rBTpL7eSLNu7i9hNAKEzpE9tUK91XGdhZe");
//        l.add("raELZmKzEwaY3bcToBGrmu9gMysZiTT7Sq");
//        l.add("rHtSL4U3B9j2QXeFELF3WoDHJ39wVs4kQn");
//        l.add("rKTymYboKF3WuDH1qVPLiW3sZmLDpwX1UR");
//        l.add("rN1TMFkY8NvzK8ADjdngfpVWe5beDKAq4t");

        Set<String> labelSet = new HashSet<String>();
        BufferedReader preReader = new BufferedReader(new FileReader("/Users/xuda/Downloads/relations_20211016-bundle-test.txt"));
        String preLine = null;
        long t1 = System.currentTimeMillis();
        while ((preLine = preReader.readLine()) != null) {
            String[] item = preLine.split(",",-1);
            if(item.length != 4){
                System.out.println("something wrong with this line: " + preLine);
            }else{
                labelSet.add(item[0]);
                labelSet.add(item[1]);
            }
        }
        Map<Integer, String> idLabelMap = new HashMap<>();//id-hash 对应表
        Map<String, Integer> labelIdMap = new HashMap<>();//hash-id 对应表
        int temp = 1;//通常从1开始编号，0留给森林情况下的虚拟根
        for(String s: labelSet){
            idLabelMap.put(temp, s);
            temp ++;
        }
        labelIdMap = idLabelMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getValue(), entry -> entry.getKey()));
        long t2 = System.currentTimeMillis();
        System.out.println("first scan time: " + (t2 - t1) / 1000 + "s");

//        String tempLabel = idLabelMap.get(43149);
//        System.out.println(tempLabel);
//        int tempId = labelIdMap.get("rMNiNsjeZ8T4a2Tst2oTFCK5YmG5Lj2Wjj");//找到根
//        System.out.println(tempId);
        for (String label : l) {
            targets.add(labelIdMap.get(label));
        }
//        for (Integer t : targets) {
//            System.out.println(t);
//        }

        preReader.close();
        /*用id建图并计算*/
        TreeGraph treeGraph = new TreeGraph();
        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/Downloads/relations_20211016-bundle-test.txt"));
        String line = null;
        Set<Integer> idSet = new HashSet<Integer>();
        t1 = System.currentTimeMillis();
        while ((line = reader.readLine()) != null) {
            String[] item = line.split(",",-1);
            if(item.length == 4){
                Integer id1 = labelIdMap.get(item[0]);
                Integer id2 = labelIdMap.get(item[1]);
                idSet.add(id1);
                idSet.add(id2);
                treeGraph.getInDegree().put(id1, 0);
                treeGraph.getOutDegree().put(id1, 0);
                treeGraph.getInDegree().put(id2, 0);
                treeGraph.getOutDegree().put(id2, 0);

                int source = id2;
                int target = id1;
                SimpleEdge simpleEdge = new SimpleEdge(source, target); // parent_id 指向 id
                //初始化NodeProperties并赋给SimpleEdge
//                NodeProperties properties = new NodeProperties();//本次计算没有属性信息
//                simpleEdge.setNodeProperties(properties);
                treeGraph.getEdgeSet().add(simpleEdge);
            }else{
                System.out.println("invalid line: " + line);
                System.out.println(" 本行列数：" + item.length);
            }
        }
        treeGraph.selfComputeInOutDegree();
        System.out.println("nodes count in file: " + idSet.size());
        System.out.println("edges count in file: " + treeGraph.getEdgeSet().size());
        reader.close();
        t2 = System.currentTimeMillis();
        System.out.println("second scan time: " + (t2 - t1) / 1000 + "s");

        treeGraph.setName("danfeng-10-18");
//        treeGraph.setIdLabelMap(idLabelMap);//新方法，设置id-label map 2022年7月12日 抽象出了 LabelTreeGraph 父类TreeGraph不再主动设置idLabelMap
//        treeGraph.detectRoot();
//        treeGraph.setRootWithLabel("2441527");//一旦设定了id-label map, 最要用这个方法设定root，而不是简单的setRoot
        treeGraph.makeBundle();//测试自动捆绑森林成树

        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );

        //查看目标人信息
        for (Integer t : targets) {
            TreeNode current = treeGraph.getTraversal().get(t);
//            System.out.println(idLabelMap.get(t) + " " + current.getLevel() + current.getBranchCount() + current.getBranchHeight() + current.getChildren().size());
            System.out.println( t + " " + idLabelMap.get(t) + " 层级：" + current.getLevel() + " 直接下线人数："+ current.getChildrenCount() +" 伞下总人数(除自身): " + (current.getBranchCount() - 1) + " 发展下线层数: " + current.getBranchHeight());
        }
        TreeNode actualRoot = treeGraph.getTraversal().get(Integer.parseInt(treeGraph.getRoots().get(0).getId()));
        System.out.println( "\n" + actualRoot.getId() + " " + idLabelMap.get(Integer.parseInt(actualRoot.getId())) + " 层级：" + actualRoot.getLevel() + " 直接下线人数："+ actualRoot.getChildrenCount() +" 伞下总人数(除自身): " + (actualRoot.getBranchCount() - 1) + " 发展下线层数: " + actualRoot.getBranchHeight());

//        treeGraph.printLongestPath("/Users/xuda/Documents/danfeng-graph-longest-path-10-18.txt");
    }
}
