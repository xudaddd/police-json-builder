import model.JsonEdge;
import model.JsonNode;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/***
 * 构造 Antv G6 Graph 需要的 nodes & edges json数据
 * @deprecated 早期试验性代码
 */
public class Builder {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/antv-json-builder/src/main/resources/q_account.csv"));
        reader.readLine();//标题信息
        String line = null;
        Set<Integer> idSet = new HashSet<Integer>();
        Set<JsonEdge> jsonEdgeSet1 = new HashSet<JsonEdge>();
        Set<JsonEdge> jsonEdgeSet2 = new HashSet<JsonEdge>();
        while((line=reader.readLine())!=null){
            String item[] = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分
            Integer id = Integer.parseInt(item[0]);
            idSet.add(id);
            String recommendedPersonId = null;
            String resettlementPeopleId = null;
            if(item.length == 2){
                recommendedPersonId = item[1];
                idSet.add(Integer.parseInt(recommendedPersonId));
                JsonEdge jsonEdge = new JsonEdge(id, Integer.parseInt(recommendedPersonId));
                jsonEdge.setLabel("parent_account_id");
                jsonEdgeSet1.add(jsonEdge);
            }
            if(item.length == 3){
                recommendedPersonId = item[1];
                idSet.add(Integer.parseInt(recommendedPersonId));
                JsonEdge jsonEdge = new JsonEdge(id, Integer.parseInt(recommendedPersonId));
                jsonEdge.setLabel("recommendedPerson");
                jsonEdgeSet1.add(jsonEdge);
                resettlementPeopleId = item[2];
                idSet.add(Integer.parseInt(resettlementPeopleId));
                jsonEdge = new JsonEdge(id, Integer.parseInt(resettlementPeopleId));
                jsonEdge.setLabel("resettlementPeople");
                jsonEdgeSet2.add(jsonEdge);
            }
        }
        List<Integer> idList = new ArrayList<Integer>(idSet);
        List<JsonNode> jsonNodeList = new ArrayList<JsonNode>();
        for(Integer id:idList){
            jsonNodeList.add(new JsonNode(id));
        }
        List<JsonEdge> jsonEdgeList1 = new ArrayList<JsonEdge>(jsonEdgeSet1);
        List<JsonEdge> jsonEdgeList2 = new ArrayList<JsonEdge>(jsonEdgeSet2);
        System.out.println("nodeSet: " + jsonNodeList.size() );
        System.out.println("edgeSet1: " + jsonEdgeSet1.size() );
        System.out.println("edgeSet2: " + jsonEdgeSet2.size() );
        BufferedWriter nodeOut = new BufferedWriter(new FileWriter("nodeJson.txt"));
        BufferedWriter edgeOut1 = new BufferedWriter(new FileWriter("edgeJson1.txt"));
        BufferedWriter edgeOut2 = new BufferedWriter(new FileWriter("edgeJson2.txt"));
        nodeOut.close();
        edgeOut1.close();
        edgeOut2.close();
    }
}
