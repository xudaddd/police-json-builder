import utils.GraphBuilder;
import model.TreeGraph;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/***
 * 测试关键字搜索
 *
 * @author xuda
 * @deprecated
 */
public class SearchTestApplication {

    public static void main(String args[]) throws FileNotFoundException {
        GraphBuilder builder = new GraphBuilder();
        TreeGraph treeGraph = new TreeGraph();
        treeGraph = builder.buildTreeGraphFromCSV(treeGraph,"properties search test", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/template/template-0.csv", 0, 6);
        treeGraph.makeBundle();//测试自动捆绑森林成树

        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );

        //特定属性搜索
//        List<TreeNode> searchResult = treeGraph.simpleSearchNodesOnProperty("用户昵称","大");
        //全属性搜索
//        List<TreeNode> searchResult = treeGraph.simpleSearchNodesOnAllProperties("156");
        //部分属性搜索
//        List<String> someProperties = new ArrayList<>();
//        someProperties.add("用户昵称");
//        someProperties.add("手机号码");
//        List<TreeNode> searchResult = treeGraph.simpleSearchNodesOnProperties(someProperties,"老");
//        for (TreeNode node : searchResult) {
//            System.out.println(node.getId() + " " + node.getNodeProperties().toString());
//        }

        //测试 寻找最近公共祖先
        List<Integer> nodeIds = new ArrayList<>();
        nodeIds.add(69777);
        nodeIds.add(48270);
        nodeIds.add(50470);
        nodeIds.add(31948);
        nodeIds.add(17711);
//        nodeIds.add(10);
        System.out.println("最近公共祖先：" + treeGraph.findLowestCommonAncestor(nodeIds));

        //测试文件格式检测
        System.out.println(builder.testFileFormat("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/template/template-0.csv"));
    }
}
