import java.io.*;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.TreeNode;
import model.SimpleEdge;


/***
 * 构造 Antv G6 TreeGraph 需要的 json 数据
 * @deprecated 早期试验性代码 验证建树图功能
 */
public class TreeBuilder {

    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<Integer, Integer> inDegree = new HashMap<Integer, Integer>();
    private static Map<Integer, Integer> outDegree = new HashMap<Integer, Integer>();
    private static Set<SimpleEdge> edgeSet = new HashSet<SimpleEdge>();
    private static Map<Integer,Set<Integer>> levelSet = new HashMap<Integer, Set<Integer>>();
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/q_account.csv"));
        reader.readLine();//标题信息
        String line = null;
        Set<Integer> idSet = new HashSet<Integer>();

        while((line=reader.readLine())!=null){
            String item[] = line.split(",");
            Integer id = Integer.parseInt(item[0]);
            idSet.add(id);
            inDegree.put(id,0);
            outDegree.put(id,0);
            String parentId = null;
            if(item.length == 2){
                parentId = item[1];
                int source = Integer.parseInt(parentId);
                int target = id;
                idSet.add(source);
                SimpleEdge jsonEdge = new SimpleEdge(source, target);
                edgeSet.add(jsonEdge);

                //source 出度 +1； target 入度 +1
                int oldOutDegree = outDegree.get(source) + 1;
                int oldInDegree = inDegree.get(target) + 1;

                outDegree.put(source, oldOutDegree);
//                System.out.println("更新出度 " + source + " " +oldOutDegree);
                inDegree.put(target, oldInDegree);
//                System.out.println("更新入度 " + target + " " +oldInDegree);
            }
        }
        System.out.println("nodes count: " + idSet.size());
        System.out.println("edges count: " + edgeSet.size());
        findRoot();

        TreeNode root = new TreeNode(0, 0,true);
        buildTree(root, true, 100);

        //层序输出
        for(Map.Entry<Integer, Set<Integer>> entry : levelSet.entrySet()) {
            Integer levelId = entry.getKey();
            Set<Integer> levelElements = entry.getValue();
            System.out.println(levelId + " :" + levelElements.toString());
        }

        String json = trimNullChildren(mapper.writeValueAsString(root));
        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("tree_parent.json"));

        jsonWriter.write(json);
        jsonWriter.close();
    }

    private static void buildTree(TreeNode root, boolean isCollapsed, int levelLimit){
        if(levelSet.get(root.getLevel()) == null){
            levelSet.put(root.getLevel(),new HashSet<Integer>());
        }
        levelSet.get(root.getLevel()).add(Integer.parseInt(root.getId()));

//        System.out.println("building tree: " + root.id);
        if(isLeaf(Integer.parseInt(root.getId()))){
//            System.out.println(root.getId() + " is leaf");
//            root.setLeaf(true);
            root.setCollapsed(false);//叶子节点永远展开
        }else{
            root.setChildren(getLeafs(Integer.parseInt(root.getId()), root.getLevel(), isCollapsed));
            if(root.getLevel() < levelLimit){
                for(TreeNode leaf: root.getChildren()){
                    buildTree(leaf, isCollapsed, levelLimit);
                }
            }
        }
    }

    private static boolean isLeaf(Integer id){
        if(outDegree.get(id).intValue() == 0){
            return true;
        }else{
            return false;
        }
    }

    private static List<TreeNode> getLeafs(Integer id, int currentLevel, boolean isCollapsed){
        List<TreeNode> l = new ArrayList<TreeNode>();
        for(SimpleEdge e: edgeSet){
            if(e.getSource().intValue() == id.intValue()){
                l.add(new TreeNode(e.getTarget(), currentLevel + 1, isCollapsed));
            }
        }
        return l;
    }

    private static void findRoot(){
        for(Map.Entry<Integer, Integer> entry : inDegree.entrySet()){
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(value.intValue() == 0){
                System.out.println("found root: " + key);
            }
        }
    }

    private static String trimNullChildren(String old){
        return old.replace(",\"children\":null", "");
    }
}
