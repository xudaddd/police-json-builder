import com.fasterxml.jackson.databind.ObjectMapper;
import model.TreeNode;
import model.SimpleEdge;

import java.io.*;
import java.util.*;


/***
 * 构造 Antv G6 TreeGraph 需要的 json 数据
 * @deprecated 早期试验性代码 验证建树图功能
 */
public class TreeBuilderRe {
    //D recommendedPerson
    //T resettlementPeople
    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<Integer, Integer> inDegree_D = new HashMap<Integer, Integer>();//入度信息
    private static Map<Integer, Integer> outDegree_D = new HashMap<Integer, Integer>();//出度信息
    private static Set<SimpleEdge> edgeSet_D = new HashSet<SimpleEdge>();
    private static Map<Integer,Set<Integer>> levelSet_D = new HashMap<Integer, Set<Integer>>();
    private static Map<Integer, Integer> inDegree_T = new HashMap<Integer, Integer>();//入度信息
    private static Map<Integer, Integer> outDegree_T = new HashMap<Integer, Integer>();//出度信息
    private static Set<SimpleEdge> edgeSet_T = new HashSet<SimpleEdge>();
    private static Map<Integer,Set<Integer>> levelSet_T = new HashMap<Integer, Set<Integer>>();
    private static int loop_D = 0;
    private static int loop_T = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/sdkj_user.csv"));
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/q_account.csv"));

        reader.readLine();//第一行标题信息
        String line = null;
        Set<Integer> idSet_D = new HashSet<Integer>();
        Set<Integer> idSet_T = new HashSet<Integer>();

        while((line=reader.readLine())!=null){
            String item[] = line.split(",");
            Integer id = Integer.parseInt(item[0]);
            idSet_D.add(id);
            idSet_T.add(id);
            String D_Id = null;
            String T_Id = null;
            inDegree_D.put(id,0);
            outDegree_D.put(id,0);
            inDegree_T.put(id,0);
            outDegree_T.put(id,0);
            if(item.length == 2){
                System.out.println( "double colume: " + line);
                D_Id = item[1];
                idSet_D.add(Integer.parseInt(D_Id));
                SimpleEdge jsonEdge = new SimpleEdge(id, Integer.parseInt(D_Id));
                edgeSet_D.add(jsonEdge);

            }
            if(item.length == 3){
                int target = id;
                D_Id = item[1];
                int source_D = Integer.parseInt(D_Id);
                idSet_D.add(source_D);
                SimpleEdge edge_D = new SimpleEdge(source_D, target);
                edgeSet_D.add(edge_D);

                T_Id = item[2];
                int source_T = Integer.parseInt(T_Id);
                idSet_T.add(source_T);
                SimpleEdge edge_T = new SimpleEdge(source_T, target);
                edgeSet_T.add(edge_T);
//                System.out.println("source_T: " + source_T);
            }
        }

        //初始化出入度
        for(SimpleEdge e : edgeSet_D){
            int oldOutDegree_D = outDegree_D.get(e.getSource()) + 1;
            int oldInDegree_D = inDegree_D.get(e.getTarget()) + 1;
            outDegree_D.put(e.getSource(), oldOutDegree_D);
            inDegree_D.put(e.getTarget(), oldInDegree_D);
        }
        for(SimpleEdge e : edgeSet_T){
            int oldOutDegree_T = outDegree_T.get(e.getSource()) + 1;
            int oldInDegree_T = inDegree_T.get(e.getTarget()) + 1;
            outDegree_T.put(e.getSource(), oldOutDegree_T);
            inDegree_T.put(e.getTarget(), oldInDegree_T);
        }

        System.out.println("D nodes count: " + idSet_D.size());
        System.out.println("D edges count: " + edgeSet_D.size());

        System.out.println("T nodes count: " + idSet_T.size());
        System.out.println("T edges count: " + edgeSet_T.size());
        findRoot_D();
        findRoot_T();


        TreeNode root_D = new TreeNode(2, 0,true);
        TreeNode root_T = new TreeNode(2, 0,true);

        buildTree_D(root_D, true,10);//默认都收起来
        buildTree_T(root_T, true,10);//默认都收起来
        System.out.println("tree_level_recommendedPerson: ");
        for(Map.Entry<Integer, Set<Integer>> entry : levelSet_D.entrySet()) {
            Integer levelId = entry.getKey();
            Set<Integer> levelElements = entry.getValue();
            System.out.println(levelId + " :" + levelElements.toString());
        }
        System.out.println("tree_level_resettlementPeople: ");
        for(Map.Entry<Integer, Set<Integer>> entry : levelSet_T.entrySet()) {
            Integer levelId = entry.getKey();
            Set<Integer> levelElements = entry.getValue();
            System.out.println(levelId + " :" + levelElements.toString());
        }

        String json_D = trimNullChildren(mapper.writeValueAsString(root_D));
        String json_T = trimNullChildren(mapper.writeValueAsString(root_T));

        BufferedWriter jsonWriter_D = new BufferedWriter(new FileWriter("tree_recommendedPerson.json"));
        BufferedWriter jsonWriter_T = new BufferedWriter(new FileWriter("tree_resettlementPeople.json"));

        jsonWriter_D.write(json_D);
        jsonWriter_D.close();

        jsonWriter_T.write(json_T);
        jsonWriter_T.close();
    }

    /***
     * D tree
     * @param root
     * @param isCollapsed
     */
    private static void buildTree_D(TreeNode root, boolean isCollapsed, int levelLimit){
//        loop_D ++;
//        if(loop_D > 29){ //buildTree执行多少次，而不是多少层
//            return;
//        }
        if(levelSet_D.get(root.getLevel()) == null){
            levelSet_D.put(root.getLevel(),new HashSet<Integer>());
        }
        levelSet_D.get(root.getLevel()).add(Integer.parseInt(root.getId()));
//        System.out.println("building tree: " + root.id);
        if(isLeaf_D(Integer.parseInt(root.getId()))){
//            System.out.println(root.getId() + " is leaf");
//            root.setLeaf(true);
            root.setCollapsed(false);//叶子节点永远展开
        }else{
            root.setChildren(getLeafs_D(Integer.parseInt(root.getId()), root.getLevel(), isCollapsed));
            if(root.getLevel() < levelLimit){
                for(TreeNode leaf: root.getChildren()){
                    buildTree_D(leaf, isCollapsed, levelLimit);
                }
            }
        }
    }

    private static boolean isLeaf_D(Integer id){
//        System.out.println(" 出度 " + id + " " + outDegree.get(id));
        if(outDegree_D.get(id).intValue() == 0){
            return true;
        }else{
            return false;
        }
    }

    private static List<TreeNode> getLeafs_D(Integer id, int currentLevel, boolean isCollapsed){
        List<TreeNode> l = new ArrayList<TreeNode>();
        for(SimpleEdge e: edgeSet_D){
            if(e.getSource().intValue() == id.intValue()){
                l.add(new TreeNode(e.getTarget(), currentLevel + 1, isCollapsed));
//                System.out.println("adding node: " + e.target);
            }
        }
//        System.out.println("adding " + id + " leaf list: " + l.size());
        return l;
    }

    /***
     * T tree
     * @param root
     * @param isCollapsed
     */
    private static void buildTree_T(TreeNode root, boolean isCollapsed, int levelLimit){
//        loop_T ++;
//        if(loop_T > 29){
//            return;
//        }
        if(levelSet_T.get(root.getLevel()) == null){
            levelSet_T.put(root.getLevel(),new HashSet<Integer>());
        }
        levelSet_T.get(root.getLevel()).add(Integer.parseInt(root.getId()));
//        System.out.println("building tree: " + root.id);
        if(isLeaf_T(Integer.parseInt(root.getId()))){
//            System.out.println(root.getId() + " is leaf");
//            root.setLeaf(true);
            root.setCollapsed(false);//叶子节点永远展开
        }else{
            root.setChildren(getLeafs_T(Integer.parseInt(root.getId()), root.getLevel(),isCollapsed));
            if(root.getLevel() < levelLimit) {
                for (TreeNode leaf : root.getChildren()) {
                    buildTree_T(leaf, isCollapsed, levelLimit);
                }
            }
        }
    }

    private static boolean isLeaf_T(Integer id){
//        System.out.println(" 出度 " + id + " " + outDegree.get(id));
        if(outDegree_T.get(id).intValue() == 0){
            return true;
        }else{
            return false;
        }
    }

    private static List<TreeNode> getLeafs_T(Integer id, int currentLevel, boolean isCollapsed){
        List<TreeNode> l = new ArrayList<TreeNode>();
        for(SimpleEdge e: edgeSet_T){
            if(e.getSource().intValue() == id.intValue()){
                l.add(new TreeNode(e.getTarget(), currentLevel + 1, isCollapsed));
//                System.out.println("adding node: " + e.target);
            }
        }
//        System.out.println("adding " + id + " leaf list: " + l.size());
        return l;
    }



    private static void findRoot_D(){
        List<Integer> root_D = new ArrayList<Integer>();
        for(Map.Entry<Integer, Integer> entry : inDegree_D.entrySet()){
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(value.intValue() == 0){
//                System.out.println("found D-root: " + key);
                if(outDegree_D.get(key).intValue() > 0) {
                    root_D.add(key);
                }
            }
        }
        System.out.println("D-roots: " + root_D.toString());
    }

    private static void findRoot_T(){
        List<Integer> root_T = new ArrayList<Integer>();
        for(Map.Entry<Integer, Integer> entry : inDegree_T.entrySet()){
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(value.intValue() == 0){
//                System.out.println("found T-root: " + key);
                if(outDegree_T.get(key).intValue() > 0) {
                    root_T.add(key);
                }
            }
        }
        System.out.println("T-roots: " + root_T.toString());
    }

    private static String trimNullChildren(String old){
        return old.replace(",\"children\":null", "");
    }
}
