package constant;

/***
 * 节点属性名称
 * @author xuda
 * @date 2021-11-08
 */
public class GraphPropertyConstants {
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_PARENT_ID = "parentId";
}
