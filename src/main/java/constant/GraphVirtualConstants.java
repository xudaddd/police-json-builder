package constant;


/***
 * 虚拟节点有关标志&常量
 * @author xuda
 */
public class GraphVirtualConstants {
    public static final String VIRTUAL_ROOT_ID = "-1";
//    public static final String VIRTUAL_ROOT_ID = "0";
    public static final String VIRTUAL_ROOT_LABEL = "root";
    public static final String VIRTUAL_NODE_PREFIX = "XN_";
}
