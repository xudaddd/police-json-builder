package constant;

/***
 * html模板内容，随用随改
 * @author xuda
 */
public class HtmlConstants {

    public static final String UP_HTML = "<!DOCTYPE html>\n" +
        "<html lang=\"en\">\n" +
        "<head>\n" +
        "    <meta charset=\"UTF-8\" />\n" +
        "    <title>Tutorial Demo</title>\n" +
        "    <style>\n" +
        "        /* 提示框的样式 */\n" +
        "        .g6-tooltip {\n" +
        "            border: 1px solid #e2e2e2;\n" +
        "            border-radius: 4px;\n" +
        "            font-size: 12px;\n" +
        "            color: #545454;\n" +
        "            background-color: rgba(255, 255, 255, 0.9);\n" +
        "            padding: 10px 8px;\n" +
        "            box-shadow: rgb(174, 174, 174) 0px 0px 10px;\n" +
        "        }\n" +
        "        #mountNode {\n" +
        "            background-color: white;\n" +
        "            width: 2000px;\n" +
        "            height: 1600px;\n" +
        "        }\n" +
        "    </style>\n" +
        "</head>\n" +
        "<body>\n" +
        "<div id=\"mountNode\"></div>\n" +
        "<script src=\"./antv.js\"></script>\n" +
        "<script>\n" +
        "    // const localData = // 本地数据粘贴到这\n";
    public static final String JSON_HEAD = "const smallLocalData = ";
    public static final String DOWN_HTML = "revealProperties(smallLocalData);\n" +
            "    const localData = smallLocalData;\n" +
            "    const button = document.createElement('button');\n" +
            "    button.innerHTML = '全部展开/收起';\n" +
            "    const container = document.getElementById('mountNode');\n" +
            "    container.appendChild(button);\n" +
            "    button.addEventListener('click', (e) => {\n" +
            "        // console.log(\"expand/collapse\")\n" +
            "        if(localData.collapsed){\n" +
            "            // let reg = new RegExp(\"true\",\"g\")//g表示全部的\n" +
            "            setCollapsed(localData, false);\n" +
            "            localData.collapsed = false;\n" +
            "        }else{\n" +
            "            // let reg = new RegExp(\"false\",\"g\")//g表示全部的\n" +
            "            setCollapsed(localData, true);\n" +
            "            localData.collapsed = true;\n" +
            "        }\n" +
            "        graph.data(localData);\n" +
            "        graph.render();\n" +
            "        graph.layout();\n" +
            "    });\n" +
            "    /***\n" +
            "     * 递归设置展开/收起属性\n" +
            "     * @param data\n" +
            "     * @param isCollapsed\n" +
            "     */\n" +
            "    function setCollapsed(data, isCollapsed){\n" +
            "        // console.log(\"children size: \" + data.children.length);\n" +
            "        // let children = data.children;\n" +
            "        if(data.children){\n" +
            "            data.collapsed = isCollapsed;\n" +
            "            for (const child in data.children) {\n" +
            "                setCollapsed(data.children[child], isCollapsed);\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "   /**\n" +
            "     * 递归显现属性信息\n" +
            "     * 提升 propertyMap 层级\n" +
            "     */\n" +
            "    function revealProperties(data){\n" +
            "        if (data.nodeProperties && data.nodeProperties.propertyMap) {\n" +
            "            let map = data.nodeProperties.propertyMap;\n" +
            "            data.nodeProperties = map;\n" +
            "        }\n" +
            "        if(data.children){\n" +
            "            for (const child in data.children) {\n" +
            "                revealProperties(data.children[child]);\n" +
            "            }\n" +
            "        }\n" +
            "    }" +
            "    // 实例化 grid 插件\n" +
            "    const grid = new G6.Grid();\n" +
            "    const graph = new G6.TreeGraph({\n" +
            "        fitView: true,\n" +
            "        container: 'mountNode',\n" +
            "        width:2000,\n" +
            "        height:1600,\n" +
            "        modes: {\n" +
            "            default: [\n" +
            "                {\n" +
            "                    type: 'collapse-expand',\n" +
            "                    onChange: function onChange(item, collapsed) {\n" +
            "                        const data = item.get('model');\n" +
            "                        data.collapsed = collapsed;\n" +
            "                        return true;\n" +
            "                    },\n" +
            "                },\n" +
            "                {\n" +
            "                        type: 'tooltip', // 提示框\n" +
            "                        offset: 50,\n" +
            "                        formatText(model) {\n" +
            "                           // 提示框文本内容，有属性的话显示全部属性，否则只显示id\n" +
            "                           if (model.nodeProperties) {\n" +
            "                               let text = 'id: ' + model.id + '<br/>';\n" +
            "                               for (let p in model.nodeProperties) {\n" +
            "                                   if (p != \"id\") {\n" +
            "                                       text += (p + \": \" + model.nodeProperties[p] + '<br/>');\n" +
            "                                   }\n" +
            "                               }\n" +
            "                               return text;\n" +
            "                           } else {\n" +
            "                               text = 'id: ' + model.id + '<br/>';\n" +
            "                               return text;\n" +
            "                           }\n" +
            "                       },\n" +
            "                },\n" +
            "                'drag-canvas',\n" +
            "                'zoom-canvas',\n" +
            "            ],\n" +
            "        },\n" +
            "        defaultNode: {\n" +
            "            size: 26,\n" +
            "            anchorPoints: [\n" +
            "                [0, 0.5],\n" +
            "                [1, 0.5],\n" +
            "            ],\n" +
            "        },\n" +
            "        defaultEdge: {\n" +
            "            type: 'cubic-horizontal',\n" +
            "        },\n" +
            "        layout: {\n" +
            "            type: 'compactBox',\n" +
            "            direction: 'LR', // H / V / LR / RL / TB / BT\n" +
            "            nodeSep: 30,\n" +
            "            rankSep: 100,\n" +
            "        },\n" +
            "        nodeStateStyles: {\n" +
            "            // 鼠标 hover 上节点，即 hover 状态为 true 时的样式\n" +
            "            hover: {\n" +
            "                fill: 'lightsteelblue',\n" +
            "            },\n" +
            "            // 鼠标点击节点，即 click 状态为 true 时的样式\n" +
            "            click: {\n" +
            "                // stroke: '#0e00ff',\n" +
            "                lineWidth: 3,\n" +
            "            },\n" +
            "        }\n" +
            "    });\n" +
            "    graph.node(function (node) {\n" +
            "        return {\n" +
            "            label: node.id, //+ \" (\" + node.level+ \")\",\n" +
            "            labelCfg: {\n" +
            "                position: node.children && node.children.length > 0 ? 'left' : 'right',\n" +
            "                offset: 5,\n" +
            "            },\n" +
            "            style:{ //leaf & branch 颜色区别\n" +
            "                fill: node.children && node.children.length > 0 ? '#EFF4FF' : '#B0D7F6',\n" +
            "                lineWidth: node.collapsed  ? 3 : 1,\n" +
            "            }\n" +
            "        };\n" +
            "    });\n" +
            "    graph.on('itemcollapsed', (e) => {\n" +
            "        // console.log(e.item);// 当前被操作的节点 item\n" +
            "        // console.log(e.collapsed);// 当前操作是收起（`true`）还是展开（`false`）\n" +
            "        if(e.collapsed){\n" +
            "            // graph.setItemState(e.item, 'click', true); // 设置当前节点的 click 状态为 true\n" +
            "            e.item.get('model').style.lineWidth = 3;\n" +
            "            e.item.refresh();\n" +
            "        }else{\n" +
            "            // graph.setItemState(e.item, 'click', false); // 设置当前节点的 click 状态为 false\n" +
            "            e.item.get('model').style.lineWidth = 1;\n" +
            "            e.item.refresh();\n" +
            "        }\n" +
            "    });\n" +
            "    // graph.on('node:click', (e) => {\n" +
            "    //     // 先将所有当前是 click 状态的节点置为非 click 状态\n" +
            "    //     const clickNodes = graph.findAllByState('node', 'click');\n" +
            "    //     clickNodes.forEach((cn) => {\n" +
            "    //         graph.setItemState(cn, 'click', false);\n" +
            "    //     });\n" +
            "    //     const nodeItem = e.item; // 获取被点击的节点元素对象\n" +
            "    //     graph.setItemState(nodeItem, 'click', true); // 设置当前节点的 click 状态为 true\n" +
            "    // });\n" +
            "    const main = async () => {\n" +
            "        const response = await fetch(\n" +
            "            // 'https://gw.alipayobjects.com/os/basement_prod/6cae02ab-4c29-44b2-b1fd-4005688febcb.json', //远程访问\n" +
            "            'https://gw.alipayobjects.com/os/antvdemo/assets/data/algorithm-category.json'\n" +
            "            // './parent-localData.json', //本地访问\n" +
            "        );\n" +
            "        const remoteData = await response.json();\n" +
            "\n" +
            "        graph.data(localData);\n" +
            "        graph.render();\n" +
            "    };\n" +
            "    main();\n" +
            "</script>\n" +
            "</body>\n" +
            "</html>";
}
