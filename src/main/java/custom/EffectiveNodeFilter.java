package custom;

import model.TreeNode;

/**
 * 有效节点过滤接口，方便添加自定义过滤条件
 * @author xuda
 * @date 2021-11-05
 */
@FunctionalInterface
public interface EffectiveNodeFilter {
    boolean doFilter(TreeNode treeNode);
}
