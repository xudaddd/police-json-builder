package investigation;

import constant.HtmlConstants;
import utils.GraphBuilder;
import model.TreeGraph;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/***
 *
 * @author xuda
 */
public class Application20211025 {
    public static void main(String[] args) throws IOException {
        GraphBuilder graphBuilder = new GraphBuilder();
        TreeGraph treeGraph = new TreeGraph();
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general("Cangwu", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/广西梧州/广西梧州市苍梧县嫌疑人及其下线人员信息-20211022.csv", 0, 3);
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph,"Jingning", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/甘肃静宁/甘肃静宁县嫌疑人及其下线人员信息-20211102.csv", 0, 3);
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );

//        String json = treeGraph.getTreeNodeJson(129880);//拿到json
        String json = treeGraph.getTreeNodeJson(12380);//拿到json
        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/甘肃静宁/甘肃静宁县嫌疑人及其下线人员信息-20211102.html"));
        String htmlContent = HtmlConstants.UP_HTML + HtmlConstants.JSON_HEAD + json + "\n" + HtmlConstants.DOWN_HTML;
        jsonWriter.write(htmlContent);
        jsonWriter.flush();
        jsonWriter.close();
    }
}
