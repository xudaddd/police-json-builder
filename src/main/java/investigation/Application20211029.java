package investigation;

import custom.EffectiveNodeFilter;
import model.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/***
 *
 *
 * 10月29日案件 慧文
 *
 * 使用三个文件：
 *  user.csv
 *  user_identity.csv
 *  relations_20211016.txt
 *
 * 启动命令：
 *  java -jar -XX:+PrintHeapAtGC -XX:+UseG1GC -XX:+UseStringDeduplication -Xms1024m -Xmx61440m -Xss128m police-json-builder.jar
 */
public class Application20211029 {

    private static final String TARGET_PATH = "/usr/dxu/java-app/ld/";
    public static void main(String[] args) throws IOException {

        //自定义的有效节点判断方法
        EffectiveNodeFilter effectiveNodeFilter = treeNode -> {
            if (treeNode.getNodeProperties() != null && treeNode.getNodeProperties().getPropertyMap().get("id") != null) {
                if (!treeNode.getNodeProperties().getPropertyMap().get("id").startsWith("XN_")) {
                    //非"XN_"开头为有效
                    return true;
                }
            }
            return false;
        };

        /***
         * 扫描user实名信息
         * user_identity.csv
         *
         */
        Map<String, NodeProperties> uidIdentityMap = new HashMap<>();//uid-identity 实名信息表
//        BufferedReader r = new BufferedReader(new FileReader("/Users/xuda/Downloads/LD层级/user_identity.csv"));
        BufferedReader r = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/user_identity.csv"));
        String identityTitleLine = r.readLine();//第一行信息，为标题信息
        String[] identityColumnName = identityTitleLine.replace("\"","").split(",");
        int identityColumnCount = identityColumnName.length;//第一行有多少个元素
        String identityLine = null;
        while ((identityLine = r.readLine()) != null) {
            String item[] = identityLine.replace("可见,","可见").replace("\"", "").split(",", -1);//不忽略空值
            if (item.length == identityColumnCount) {
                //借用节点属性数据结构
                NodeProperties properties = new NodeProperties();
                for(int i = 0; i < identityColumnCount; i ++){
                    properties.saveProperty(identityColumnName[i], item[i]);
                }
                uidIdentityMap.put(item[1], properties);
            } else {
                System.out.println("异常 identity line length: " + item.length + " " + identityLine);
            }
        }
        System.out.println("scan user_identity.csv done");

        /***
         * 扫描 user 信息
         * user.csv中有的字段包含',' 有的有'\n' 把这些异常行输出到user_error_lines.csv并人工矫正，去掉所有的换行符，所有的逗号变成'.'
         * user_error_lines 更名为 user_correction_lines 与 user.csv 一起作为user信息的输入
         * 观测发现 user 里面有 address 为空的情况
         */
        //属性中排除的列
        Set<String> excludeColumns = new HashSet<>();
//        excludeColumns.add("password");
        Map<String, NodeProperties> addressPropertiesMap = new HashMap<>();//address-properties， user 中 address 不为空的
        Map<Integer, NodeProperties> nullAddressPropertiesMap = new HashMap<>();//uid-properties，user 中 address 为空的
        Set<Integer> uidsInHierarchy = new HashSet<>();//所有在层级中出现过的uid
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/Downloads/LD层级/user.csv"));
        BufferedReader reader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/user.csv"));
        String titleLine = reader.readLine();//第一行信息，为标题信息
        String[] columnName = titleLine.replace("\"","").split(",");
        int columnCount = columnName.length;//第一行有多少个元素
        String line = null;
        long t1 = System.currentTimeMillis();
        while ((line = reader.readLine()) != null) {
            String item[] = line.replace("\"","").split(",", -1);//不忽略空值
            if (item.length == columnCount) {
                NodeProperties properties = new NodeProperties();
                for(int i = 0; i < columnCount; i ++){
                    if (!excludeColumns.contains(columnName[i])) {
                        properties.saveProperty(columnName[i], item[i]);
                    }
                }
                if (item[5] != null && !item[5].equals("")) {
                    addressPropertiesMap.put(item[5], properties);
                } else {
                    nullAddressPropertiesMap.put(Integer.parseInt(item[0]), properties);
                }
            } else {
//                System.out.println("异常 line: ");
//                System.out.println(" " + line);
//                txtWriter.write(line + "\n");
            }
        }
        reader.close();
        System.out.println("...正常读user结束");
//        reader = new BufferedReader(new FileReader("/Users/xuda/Downloads/LD层级/user_correction_lines.csv"));
        reader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/user_correction_lines.csv"));
        while ((line = reader.readLine()) != null) {
            String item[] = line.replace("\"","").split(",", -1);//不忽略空值
            if (item.length == columnCount) {
                NodeProperties properties = new NodeProperties();
                for(int i = 0; i < columnCount; i ++){
                    if (!excludeColumns.contains(columnName[i])) {
                        properties.saveProperty(columnName[i], item[i]);
                    }
                }
                if (item[5] != null && !item[5].equals("")) {
                    addressPropertiesMap.put(item[5], properties);
                } else {
                    nullAddressPropertiesMap.put(Integer.parseInt(item[0]), properties);
                }
            } else {
                System.out.println("异常 correction line : ");
                System.out.println(" " + line);
            }
        }
        reader.close();
        System.out.println("...补充读user结束");
        long t2 = System.currentTimeMillis();
        System.out.println("scan user.csv time: " + (t2 - t1) / 1000 + "s");
        System.gc();

        /**
         * 建立传销参与者id-label（hash）映射
         *
         */
        Set<String> labelSet = new HashSet<>();
//        BufferedReader preReader = new BufferedReader(new FileReader("/Users/xuda/Downloads/relations_20211016.txt"));
        BufferedReader preReader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/relations_20211016.txt"));
        String preLine = null;
        t1 = System.currentTimeMillis();
        while ((preLine = preReader.readLine()) != null) {
            String[] item = preLine.split(",",-1);
            if(item.length != 4){
                System.out.println("something wrong with this line: " + preLine);
            }else{
                labelSet.add(item[0]);
                labelSet.add(item[1]);
            }
        }
        preReader.close();
        Map<Integer, String> idLabelMap = new HashMap<>();//id-hash 对应表
        Map<String, Integer> labelIdMap;//hash-id 对应表
        int temp = 1;//通常从1开始编号，0留给森林情况下的虚拟根
        for(String s: labelSet){
            idLabelMap.put(temp, s);
            temp ++;
        }
        labelIdMap = idLabelMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getValue(), entry -> entry.getKey()));
        t2 = System.currentTimeMillis();
        System.out.println("first scan relations time: " + (t2 - t1) / 1000 + "s");

        /***
         * 建立传销层级树图
         * 2022年7月12日，抽象出 LabelTreeGraph方法，以后案件推荐使用
         */
        LabelTreeGraph treeGraph = new LabelTreeGraph();
//        reader = new BufferedReader(new FileReader("/Users/xuda/Downloads/relations_20211016.txt"));
        reader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/relations_20211016.txt"));
        Set<Integer> idSet = new HashSet<Integer>();
        int xnBase = 0;
        t1 = System.currentTimeMillis();
        while ((line = reader.readLine()) != null) {
            String[] item = line.split(",",-1);
            if(item.length == 4){
                Integer id1 = labelIdMap.get(item[0]);
                Integer id2 = labelIdMap.get(item[1]);
                idSet.add(id1);
                idSet.add(id2);
                treeGraph.getInDegree().put(id1, 0);
                treeGraph.getOutDegree().put(id1, 0);
                treeGraph.getInDegree().put(id2, 0);
                treeGraph.getOutDegree().put(id2, 0);

                int source = id2;
                int target = id1;
                SimpleEdge simpleEdge = new SimpleEdge(source, target);
                //设置uid
                if (addressPropertiesMap.containsKey(item[0])) {
                    simpleEdge.setNodeProperties(addressPropertiesMap.get(item[0]));
                } else {
                    //注意属性信息中的id和树图节点的id不同，我们用属性信息中的id表示uid
                    NodeProperties nodeProperties = new NodeProperties();
                    nodeProperties.saveProperty("id", "XN_" + xnBase);
                    xnBase ++;
                    simpleEdge.setNodeProperties(nodeProperties);
                }
                treeGraph.getEdgeSet().add(simpleEdge);
            }else{
                System.out.println("invalid line: " + line);
                System.out.println(" 本行列数：" + item.length);
            }
        }
        treeGraph.selfComputeInOutDegree();
        System.out.println("nodes count in file: " + idSet.size());
        System.out.println("edges count in file: " + treeGraph.getEdgeSet().size());
        reader.close();
        t2 = System.currentTimeMillis();
        System.out.println("second scan relations time: " + (t2 - t1) / 1000 + "s");

        treeGraph.setName("huiwen-10-29");
        treeGraph.setIdLabelMap(idLabelMap);//新方法，设置id-label map
//        treeGraph.makeBundleTemp(addressPropertiesMap, xnBase);//测试自动捆绑森林成树 2022年7月12日起弃用

        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );

        startTime = System.currentTimeMillis();
        treeGraph.traverseEffectiveTree(treeGraph.getRoots().get(0), effectiveNodeFilter);
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );
        System.out.println("user中 有 地址人数：" + addressPropertiesMap.size());
        System.out.println("user中 没有 地址人数：" + nullAddressPropertiesMap.size());


        /***
         * 输出完整计算结果
         * 输出顺序：user表信息，实名信息，计算属性
         *
         */
//        BufferedWriter txtWriter = new BufferedWriter(new FileWriter("/usr/dxu/java-app/ld/summary_test.csv"));
//        String resultColumn = "";
//        for (String a : columnName) {
//            resultColumn += (a + ",");
//        }
//        for (String b : identityColumnName) {
//            resultColumn += (b + ",");
//        }
//        resultColumn += "tree-level,";
//        resultColumn += "tree-branchHeight,";
//        resultColumn += "tree-branchCount,";
//        resultColumn += "tree-effectiveBranchCount,";
//        resultColumn += "tree-childrenCount,";
//        resultColumn += "tree-effectiveChildrenCount,";
//        resultColumn += "tree-parentAddr,";
//        //path 字段过于庞大先不算
////        resultColumn += "tree-path,";
//        resultColumn += "\n";
//        txtWriter.write(resultColumn);
//
//        //日期列名
//        Set<String> dateColumns = new HashSet<>();
//        dateColumns.add("identity_created_at");
//        dateColumns.add("identity_updated_at");
//        dateColumns.add("created_at");
//        dateColumns.add("updated_at");
//        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        //1.所有层级信息的
//        for (Map.Entry<Integer, TreeNode> node : treeGraph.getTraversal().entrySet()) {
//            String output = "";
//            String uid = node.getValue().getNodeProperties().getPropertyMap().get("id");
//            if (uid != null) {
//
//                //构建user字段
//                if (uid.startsWith("XN_")) {
//                    //虚拟id
//                    for (String a : columnName) {
//                        if (a.equals("id")) {
//                            output += (uid + ",");
//                        } else {
//                            output += ("" + ",");
//                        }
//                    }
//                } else {
//                    //有效id
//                    uidsInHierarchy.add(Integer.parseInt(uid));//挑出所有在层级中的uid
//                    for (String a : columnName) {
//                        String current = node.getValue().getNodeProperties().getPropertyMap().get(a);
//                        if (!dateColumns.contains(a)) {
//                            output += ( current + ",");
//                        } else {
//                            if (current != null && current != "") {
//                                Date date = new Date(Long.parseLong(current));
//                                output += (formatFull.format(date) + ",");
//                            } else {
//                                output += ("" + ",");
//                            }
//                        }
//                    }
//                }
//
//                //构建user_identity字段
//                if (uidIdentityMap.containsKey(uid)) {
//                    //包含实名信息
//                    for (String a : identityColumnName) {
////                        output += (uidIdentityMap.get(uid).getPropertyMap().get(a) + ",");
//                        String current = uidIdentityMap.get(uid).getPropertyMap().get(a);
//                        if (!dateColumns.contains(a)) {
//                            output += (current + ",");
//                        } else {
//                            if (current != null && current != "") {
//                                try {
//                                    Date date = new Date(Long.parseLong(current));
//                                    output += (formatFull.format(date) + ",");
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    System.out.println("date content exception: " + current);
//                                    output += ("" + ",");
//                                }
//                            } else {
//                                output += ("" + ",");
//                            }
//                        }
//                    }
//                } else {
//                    //不包含实名信息
//                    for (String a : identityColumnName) {
//                        output += ("" + ",");
//                    }
//                }
//
//                //构建计算字段
//                output += node.getValue().getLevel() + ",";
//                output += node.getValue().getBranchHeight() + ",";
//                output += node.getValue().getBranchCount() + ",";
//                output += node.getValue().getEffectiveBranchCount() + ",";
//                output += node.getValue().getChildrenCount() + ",";
//                output += node.getValue().getEffectiveChildrenCount(effectiveNodeFilter) + ",";
//                if (node.getValue().getParentId() != null) {
//                    output += idLabelMap.get(Integer.parseInt(treeGraph.getTraversal().get(Integer.parseInt(node.getValue().getParentId())).getId())) + ",";
//                } else {
//                    output += "" + ",";
//                }
////                if (!node.getValue().getNodeProperties().getPropertyMap().get("id").startsWith("XN_")) {
////                    output += treeGraph.getPathStringFromRoot(node.getKey(), "id", "->") + ",";
////                } else {
////                    output += "XN_PATH" + ",";
////                }
//            } else {
//                System.out.println("null NodeProperties or null uid, node's info: " + node.getValue().getId() +
//                        " id:(uid) " + idLabelMap.get(node.getValue().getId()) +
////                        "propertyMap: " + addressPropertiesMap.get(idLabelMap.get(node.getValue().getId())).getPropertyMap() +
//                        " level: " + node.getValue().getLevel() +
//                        " childrenCount: " + node.getValue().getChildrenCount() +
//                        " branchHeight: " + node.getValue().getBranchHeight() +
//                        " effectiveBranchCount: " + node.getValue().getEffectiveBranchCount() +
//                        " branchCount: " + node.getValue().getBranchCount());
//            }
//            output += "\n";
//            txtWriter.write(output);
//        }
//
//        //2.没有地址的，不过有可能有实名信息的
//        for (NodeProperties property : nullAddressPropertiesMap.values()) {
//            String output = "";
//            String uid = property.getPropertyMap().get("id");
//
//            //构建user字段
//            for (String a : columnName) {
////                output += (property.getPropertyMap().get(a) + ",");
//                String current = property.getPropertyMap().get(a);
//                if (!dateColumns.contains(a)) {
//                    output += ( current + ",");
//                } else {
//                    if (current != null && current != "") {
//                        Date date = new Date(Long.parseLong(current));
//                        output += (formatFull.format(date) + ",");
//                    } else {
//                        output += ("" + ",");
//                    }
//                }
//            }
//
//            //构建user_identity字段
//            if (uidIdentityMap.containsKey(uid)) {
//                //包含实名信息
//                for (String a : identityColumnName) {
////                    output += (uidIdentityMap.get(uid).getPropertyMap().get(a) + ",");
//                    String current = uidIdentityMap.get(uid).getPropertyMap().get(a);
//                    if (!dateColumns.contains(a)) {
//                        output += (current + ",");
//                    } else {
//                        if (current != null && current != "") {
//                            Date date = new Date(Long.parseLong(current));
//                            output += (formatFull.format(date) + ",");
//                        } else {
//                            output += ("" + ",");
//                        }
//                    }
//                }
//            } else {
//                //不包含实名信息
//                for (String a : identityColumnName) {
//                    output += ("" + ",");
//                }
//            }
//
//            //构建计算字段
//            output += ",";
//            output += ",";
//            output += ",";
//            output += ",";
//            output += ",";
//            output += ",";
//            output += ",";
////            output += ",";
//            output += "\n";
//            txtWriter.write(output);
//        }
//
//        //3.有地址不过不曾出现在层级信息中（之前忘了这部分）
//        for (Map.Entry<String, NodeProperties> entry : addressPropertiesMap.entrySet()) {
//            String uid = entry.getValue().getPropertyMap().get("id");
//            Integer integerUid = Integer.parseInt(uid);
//            if (!uidsInHierarchy.contains(integerUid)) {
//                String output = "";
//
//                //构建user字段
//                for (String a : columnName) {
////                output += (property.getPropertyMap().get(a) + ",");
//                    String current = entry.getValue().getPropertyMap().get(a);
//                    if (!dateColumns.contains(a)) {
//                        output += ( current + ",");
//                    } else {
//                        if (current != null && current != "") {
//                            Date date = new Date(Long.parseLong(current));
//                            output += (formatFull.format(date) + ",");
//                        } else {
//                            output += ("" + ",");
//                        }
//                    }
//                }
//
//                //构建user_identity字段
//                if (uidIdentityMap.containsKey(uid)) {
//                    //包含实名信息
//                    for (String a : identityColumnName) {
////                    output += (uidIdentityMap.get(uid).getPropertyMap().get(a) + ",");
//                        String current = uidIdentityMap.get(uid).getPropertyMap().get(a);
//                        if (!dateColumns.contains(a)) {
//                            output += (current + ",");
//                        } else {
//                            if (current != null && current != "") {
//                                try {
//                                    Date date = new Date(Long.parseLong(current));
//                                    output += (formatFull.format(date) + ",");
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    System.out.println("date content exception: " + current);
//                                    output += ("" + ",");
//                                }
//                            } else {
//                                output += ("" + ",");
//                            }
//                        }
//                    }
//                } else {
//                    //不包含实名信息
//                    for (String a : identityColumnName) {
//                        output += ("" + ",");
//                    }
//                }
//
//                //构建计算字段
//                output += ",";
//                output += ",";
//                output += ",";
//                output += ",";
//                output += ",";
//                output += ",";
//                output += ",";
////            output += ",";
//                output += "\n";
//                txtWriter.write(output);
//            }
//        }
//        txtWriter.flush();
//        txtWriter.close();

        /***
         *
         * 扫描刚刚生成的文件，统计user数量
         */
//        System.out.println("checking result now...");
//        BufferedReader checkReader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/summary_test.csv"));
//        String checkTitleLine = checkReader.readLine();//第一行信息，为标题信息
//        String[] checkColumnName = checkTitleLine.split(",", -1);//不忽略空值
//        int checkColumnCount = checkColumnName.length;//第一行有多少个元素
//        String checkLine = null;
//        int xnCount = 0;
//        int validCount = 0;
//        while ((checkLine = checkReader.readLine()) != null) {
//            String item[] = checkLine.split(",", -1);//不忽略空值
//            if (item.length == checkColumnCount) {
//                if (item[0].startsWith("XN_")) {
//                    xnCount ++;
//                } else {
//                    validCount++;
//                }
//            } else {
//                System.out.println("异常 summary line: " + checkLine);
//            }
//        }
//        System.out.println("xnCount: " + xnCount);
//        System.out.println("validCount (including root(0)): " + validCount);
//        checkReader.close();


        /***
         * 已知用户，查询其伞下所有人信息，用户必有hash，否则无法出现在传销层级中
         * 两种情况：
         *  已知用户uid：通过 addressPropertiesMap 找到其hash，通过labelIdMap找到TreeGraph中的id（TreeNode的成员,不是属性中的id字段）
         *  已知用户hash：直接通过labelIdMap找到TreeGraph中的id
         * 然后使用类似 buildExcelOutput20210816 的递归方法，生成该用户的信息文件
         *
         */
//        List<String> uids = new ArrayList<>();
//        uids.add("8188354");
//        uids.add("2543207");
//        uids.add("14023638");
//        for (String uid : uids) {
//            List<String> addrs = getAddress(uid, addressPropertiesMap);
//            //先取第一个addr测试
//            if (addrs.size() > 0) {
//                for (String addr : addrs) {
//                    String path = TARGET_PATH + uid + "_" + addr + ".csv";
//                    generateReport(path, treeGraph.getDescendantsList(treeGraph.getTraversal().get(labelIdMap.get(addr))), columnName, identityColumnName, uidIdentityMap, idLabelMap, treeGraph, effectiveNodeFilter);
//                }
//            } else {
//                System.out.println("找不到 addr");
//            }
//        }


    }

    /***
     * 获取 uid 对应的 address 哈希
     * @param uid
     * @param addressPropertiesMap 同名变量
     * @return
     */
    private static List<String> getAddress(String uid, Map<String, NodeProperties> addressPropertiesMap){
        List<String> addrList = new ArrayList<>();
        for (Map.Entry<String, NodeProperties> entry : addressPropertiesMap.entrySet()) {
            if (entry.getValue().getPropertyMap().get("id").equals(uid)) {
                addrList.add(entry.getKey());
            }
        }
        //正常一个uid只对应一个地址
        System.out.println(uid + "'s addrs: " + addrList.toString());
        return addrList;
    }

    /***
     * 生成csv格式报告
     * @param path
     * @param nodes
     * @param columnName 同名变量
     * @param identityColumnName 同名变量
     * @param uidIdentityMap 同名变量
     * @throws IOException
     */
    private static void generateReport(String path,
                                       List<TreeNode> nodes,
                                       String[] columnName,
                                       String[] identityColumnName,
                                       Map<String, NodeProperties> uidIdentityMap,
                                       Map<Integer, String> idLabelMap,
                                       TreeGraph treeGraph,
                                       EffectiveNodeFilter filter) throws IOException {
        Set<String> dateColumns = new HashSet<>();
        dateColumns.add("identity_created_at");
        dateColumns.add("identity_updated_at");
        dateColumns.add("created_at");
        dateColumns.add("updated_at");
        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        BufferedWriter txtWriter = new BufferedWriter(new FileWriter(path));
        String resultColumn = "";
        for (String a : columnName) {
            resultColumn += (a + ",");
        }
        for (String b : identityColumnName) {
            resultColumn += (b + ",");
        }
        resultColumn += "tree-level,";
        resultColumn += "tree-branchHeight,";
        resultColumn += "tree-branchCount,";
        resultColumn += "tree-effectiveBranchCount,";
        resultColumn += "tree-childrenCount,";
        resultColumn += "tree-effectiveChildrenCount,";
        resultColumn += "tree-parentAddr,";
        //path 字段过于庞大先不算
//        resultColumn += "tree-path,";
        resultColumn += "\n";
        txtWriter.write(resultColumn);
        for ( TreeNode node : nodes) {
            String output = "";
            String uid = node.getNodeProperties().getPropertyMap().get("id");
            if (uid != null) {

                //构建user字段
                if (uid.startsWith("XN_")) {
                    //虚拟id
                    for (String a : columnName) {
                        if (a.equals("id")) {
                            output += (uid + ",");
                        } else {
                            output += ("" + ",");
                        }
                    }
                } else {
                    //有效id
                    for (String a : columnName) {
                        String current = node.getNodeProperties().getPropertyMap().get(a);
                        if (!dateColumns.contains(a)) {
                            output += ( current + ",");
                        } else {
                            if (current != null && current != "") {
                                Date date = new Date(Long.parseLong(current));
                                output += (formatFull.format(date) + ",");
                            } else {
                                output += ("" + ",");
                            }
                        }
                    }
                }

                //构建user_identity字段
                if (uidIdentityMap.containsKey(uid)) {
                    //包含实名信息
                    for (String a : identityColumnName) {
//                        output += (uidIdentityMap.get(uid).getPropertyMap().get(a) + ",");
                        String current = uidIdentityMap.get(uid).getPropertyMap().get(a);
                        if (!dateColumns.contains(a)) {
                            output += (current + ",");
                        } else {
                            if (current != null && current != "") {
                                try {
                                    Date date = new Date(Long.parseLong(current));
                                    output += (formatFull.format(date) + ",");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println("date content exception: " + current);
                                    output += ("" + ",");
                                }
                            } else {
                                output += ("" + ",");
                            }
                        }
                    }
                } else {
                    //不包含实名信息
                    for (String a : identityColumnName) {
                        output += ("" + ",");
                    }
                }

                //构建计算字段
                output += node.getLevel() + ",";
                output += node.getBranchHeight() + ",";
                output += node.getBranchCount() + ",";
                output += node.getEffectiveBranchCount() + ",";
                output += node.getChildrenCount() + ",";
                output += node.getEffectiveChildrenCount(filter) + ",";
                if (node.getParentId() != null) {
                    output += idLabelMap.get(Integer.parseInt(treeGraph.getTraversal().get(Integer.parseInt(node.getParentId())).getId())) + ",";
                } else {
                    output += "" + ",";
                }

            } else {
                System.out.println("null NodeProperties or null uid, node's info: " + node.getId() +
//                        " id:(uid) " + idLabelMap.get(node.getId()) +
//                        "propertyMap: " + addressPropertiesMap.get(idLabelMap.get(node.getValue().getId())).getPropertyMap() +
                        " level: " + node.getLevel() +
                        " childrenCount: " + node.getChildrenCount() +
                        " branchHeight: " + node.getBranchHeight() +
                        " effectiveBranchCount: " + node.getEffectiveBranchCount() +
                        " branchCount: " + node.getBranchCount());
            }
            output += "\n";
            txtWriter.write(output);
        }

        txtWriter.flush();
        txtWriter.close();
    }
}
