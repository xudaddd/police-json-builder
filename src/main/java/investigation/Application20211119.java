package investigation;

import java.io.*;

public class Application20211119 {
    public static void main(String[] args) throws IOException {
//        File file = new File("/Users/xuda/Downloads/20211117");
        File file = new File("/Users/xuda/Documents/雷达数据/20211201/");
        File[] files = file.listFiles();
        System.out.println();
//        BufferedWriter txtWriter = new BufferedWriter(new FileWriter("/Users/xuda/Downloads/20211117/exh.csv"));
        BufferedWriter txtWriter = new BufferedWriter(new FileWriter("/Users/xuda/Documents/雷达数据/20211201/exh.csv"));
        String resultColumn = "";
        resultColumn += "address,";
        resultColumn += "持币发行值,";
        resultColumn += "推广发行值,";
        resultColumn += "交易手续费,";
        resultColumn += "vrp发行数值,";
        resultColumn += "vbc发行数值";
        resultColumn += "\n";
        txtWriter.write(resultColumn);
        for (int i = 0; i < files.length; i ++) {
            BufferedReader reader = new BufferedReader(new FileReader(files[i]));
            String line = reader.readLine();//第一行信息，为标题信息
            while ((line = reader.readLine()) != null) {
                if(line != "\n") {
                    txtWriter.write(line + "\n");
                } else {
                    System.out.println("换行");
                }
            }
            reader.close();
        }
        txtWriter.flush();
        txtWriter.close();
    }
}
