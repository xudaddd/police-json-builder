package investigation;

import model.TreeGraph;
import utils.GraphBuilder;

import java.io.IOException;

public class Application20220117 {

    public static void main(String[] args) throws IOException {
        GraphBuilder graphBuilder = new GraphBuilder();
        TreeGraph treeGraph = new TreeGraph();
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph, "duotaibi", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/多泰币所有钱包账号记录.csv", 0, 3);
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );
        treeGraph.saveAllBranchInfo("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/多泰币-result.csv");
    }
}
