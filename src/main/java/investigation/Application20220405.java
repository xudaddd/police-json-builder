package investigation;

import constant.HtmlConstants;
import model.TreeGraph;
import utils.GraphBuilder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


/***
 * taylor 案件
 * 原始文件只有 hash 地址，解决办法是先把原始文件转成我们需要的文件格式，再计算
 */
public class Application20220405 {

    public static void main(String[] args) throws IOException {

        /***
         * 预处理，转换&输出分析需要的文件
         */
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/xuda/Documents/传销相关/somechain-0405/2-somechain.csv"));
//        Map<String, Integer> hashIdMap = new HashMap<>();
//        // 扫描原始文件，只要 status=1 的
//        String titleLine = reader.readLine();
//        String[] columnName = titleLine.split(",");
//        int columnCount = columnName.length;
//        String line = null;
//        Set<String> hashSet = new HashSet<>();
//        Set<TreeEdge> treeEdgeSet = new HashSet<>();
//        while ((line = reader.readLine()) != null) {
//            String[] segs = splitLine(line, true);
//            if (segs.length == columnCount && segs[8].equals("1")) {
//                String startAddr = segs[10];
//                String endAddr = segs[4];
//                hashSet.add(startAddr);
//                hashSet.add(endAddr);
//                TreeEdge edge = new TreeEdge(startAddr, endAddr);
//                treeEdgeSet.add(edge);
//            } else {
//                System.out.println("invalid line: " + segs[0]);
//                System.out.println(" 本行列数：" + segs.length);
//            }
//        }
//        //为addr编号
//        int index = 1;
//        for (String s : hashSet) {
//            hashIdMap.put(s, index);
//            index++;
//        }
//        System.out.println("hashSet size: " + hashSet.size());
//        System.out.println("treeEdgeSet size: " + treeEdgeSet.size());
//        System.out.println("hashIdMap size: " + hashIdMap.size());
//
//        //生成我们需要的格式
//        BufferedWriter txtWriter = new BufferedWriter(new FileWriter("/Users/xuda/Documents/传销相关/somechain-0405/2-somechain-format.csv"));
//        txtWriter.write("id, parentId, addr, parentAddr\n");
//        for (TreeEdge edge : treeEdgeSet) {
//            String formatLine = String.format("%s,%s,%s,%s\n", hashIdMap.get(edge.getTarget()), hashIdMap.get(edge.getSource()), edge.getTarget(), edge.getSource());
//            txtWriter.write(formatLine);
//        }
//        txtWriter.flush();
//        txtWriter.close();


        /***
         * 传销分析
         */
        GraphBuilder graphBuilder = new GraphBuilder();
        TreeGraph treeGraph = new TreeGraph();
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph,"somechain", "/Users/xuda/Documents/传销相关/somechain-0405/2-somechain-format.csv", 0, 1);
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );
        treeGraph.saveAllBranchInfo("/Users/xuda/Documents/传销相关/somechain-0405/2-somechain-result.csv");

        //补三个 qualified root 的 addr 属性
//        treeGraph.getTraversal().get(538).getNodeProperties().getPropertyMap().put("addr", "0tgbXk5TUSF7txvLreN9Wb78opwLHBy2zuCwUNaNxLM");
//        treeGraph.getTraversal().get(19440).getNodeProperties().getPropertyMap().put("addr", "0tJFrzfBRHsRSmGzwRFH4jwUZy852Ygm7UXfKfRY7dH");
//        treeGraph.getTraversal().get(22250).getNodeProperties().getPropertyMap().put("addr", "0t4RLsRs3EWcfh9dCSc8BuSPpvgww18TYWiYaf87H4p");

        /***
         * 生成交互层级图
         */
        String json = treeGraph.getTreeNodeJson(-1);//拿到json
        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("/Users/xuda/Documents/传销相关/somechain-0405/2-somechain.html"));
        String htmlContent = HtmlConstants.UP_HTML + HtmlConstants.JSON_HEAD + json + "\n" + HtmlConstants.DOWN_HTML;
        jsonWriter.write(htmlContent);
        jsonWriter.flush();
        jsonWriter.close();

    }

    private static String[] splitLine(String line, boolean isDoubleQuote) {
        if (!isDoubleQuote) {
            //单引号分隔
            String[] result = line.split(",", -1);
            return result;
        } else {
            //双引号分隔
            String[] segs = line.split(",",-1);
            String[] result = new String[segs.length];
            for (int i = 0; i < segs.length; i ++) {
                result[i] = segs[i].replaceAll("\"", "");
            }
            return result;
        }
    }
}
