package investigation;

import model.TreeGraph;
import utils.GraphBuilder;

import java.io.IOException;

/**
 * 曹文静 trx传销案件
 */
public class Application20220602 {
    public static void main(String[] args) throws IOException {
        GraphBuilder graphBuilder = new GraphBuilder();
        TreeGraph treeGraph = new TreeGraph();
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph,"lb", "/Users/xuda/Documents/传销相关/层级数据20220602.csv", 1, 3);
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );
        treeGraph.saveAllBranchInfo("/Users/xuda/Documents/传销相关/层级数据20220602-result.csv");
    }
}