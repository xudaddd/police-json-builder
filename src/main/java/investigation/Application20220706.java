package investigation;

import model.LabelTreeGraph;
import utils.GraphBuilder;

import java.io.IOException;
import java.util.Map;

/**
 *
 * 闫龙登 trx传销案件
 */
public class Application20220706 {
    public static void main(String[] args) throws IOException {
        /***
         * 预处理，转换&输出分析需要的文件
         */
        GraphBuilder graphBuilder = new GraphBuilder();
        Map<Integer, String> idHashMap = graphBuilder.generateFileWithId("/Users/xuda/Documents/TXi.csv", "/Users/xuda/Documents/传销相关/TXi.csv", 0, 1);
//        Map<Integer, String> idHashMap = graphBuilder.generateFileWithId("/Users/xuda/Documents/TTh.csv", "/Users/xuda/Documents/传销相关/TTh.csv", 0, 1);
//        Map<Integer, String> idHashMap = graphBuilder.generateFileWithId("/Users/xuda/Documents/TDf.csv", "/Users/xuda/Documents/传销相关/TDf.csv", 0, 1);


        /**
         * 生成层级图
         */
        LabelTreeGraph treeGraph = new LabelTreeGraph();
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph,"longdeng", "/Users/xuda/Documents/传销相关/TXi.csv", 0, 1);
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV("longdeng", "/Users/xuda/Documents/传销相关/TTh.csv", 0, 1);
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV("longdeng", "/Users/xuda/Documents/传销相关/TDf.csv", 0, 1);
        treeGraph.setIdLabelMap(idHashMap);
        treeGraph.setTopLevelNodeLabelColumnIndex(2);
        treeGraph.multiParentsDetect();
        treeGraph.loopDetect();
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );

//        String json = treeGraph.getTreeNodeJson(-1);//TXi
//        String json = treeGraph.getTreeNodeJson(-1);//TDf
//        String json = treeGraph.getTreeNodeJson(1458);//TTh
//        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("/Users/xuda/Documents/传销相关/TXi.html"));
//        treeGraph.saveAllBranchInfo("/Users/xuda/Documents/传销相关/TXi-summary.csv");
//        String htmlContent = HtmlConstants.UP_HTML + HtmlConstants.JSON_HEAD + json + "\n" + HtmlConstants.DOWN_HTML;
//        jsonWriter.write(htmlContent);
//        jsonWriter.flush();
//        jsonWriter.close();

    }
}