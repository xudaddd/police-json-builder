package investigation;

import constant.HtmlConstants;
import model.TreeGraph;
import utils.GraphBuilder;

import java.io.*;

/***
 * 生成样例数据
 */
public class ExampleGenerator {

    public static void main(String[] args) throws IOException {
        GraphBuilder graphBuilder = new GraphBuilder();
//        TreeGraph treeGraph = graphBuilder.buildTreeGraphFromCSV_general("Cangwu", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/广西梧州/广西梧州市苍梧县嫌疑人及其下线人员信息-20211022.csv", 0, 3);
        TreeGraph treeGraph = new TreeGraph();
        treeGraph = graphBuilder.buildTreeGraphFromCSV(treeGraph,"Jingning", "/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/original_example.csv", 0, 1);
        treeGraph.detectRoot();
        treeGraph.makeBundle();
        long startTime = System.currentTimeMillis();
        treeGraph.selfBuildTree(0,true, 0);//全图
        treeGraph.detectRoot();
        long endTime = System.currentTimeMillis();
        System.out.println("build tree time cost: " + (endTime - startTime) / 1000 + "s" );
        startTime = System.currentTimeMillis();
        treeGraph.traverseTree(treeGraph.getRoots().get(0));
        endTime = System.currentTimeMillis();
        System.out.println("traverse tree time cost: " + (endTime - startTime) / 1000 + "s" );

//        String json = treeGraph.getTreeNodeJson(129880);//拿到json
        String json = treeGraph.getTreeNodeJson(30536);//拿到json
        BufferedWriter jsonWriter = new BufferedWriter(new FileWriter("/Users/xuda/IdeaProjects/police-json-builder/src/main/resources/example.html"));
        String htmlContent = HtmlConstants.UP_HTML + HtmlConstants.JSON_HEAD + json + "\n" + HtmlConstants.DOWN_HTML;
        jsonWriter.write(htmlContent);
        jsonWriter.flush();
        jsonWriter.close();
    }
}
