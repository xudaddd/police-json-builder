package investigation;

import java.io.*;

/**
 * Application20211029 生成结果 行数统计
 * @author xuda
 */
public class HashEncrypter {
    public static void main(String[] args) throws IOException {
        BufferedReader checkReader = new BufferedReader(new FileReader("/Users/xuda/Downloads/LD层级/relations_20211016.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/xuda/Downloads/LD层级/relations_20211016_encrypted.txt"));
//        String checkTitleLine = checkReader.readLine();//第一行信息，为标题信息
//        String[] checkColumnName = checkTitleLine.split(",", -1);
//        int checkColumnCount = checkColumnName.length;//第一行有多少个元素
//        System.out.println("checkColumnCount: " + checkColumnCount);
        String line = null;
        while ((line = checkReader.readLine()) != null) {
            String item[] = line.split(",", -1);//不忽略空值
            String addr1 = encrypt(item[0]);
            String addr2 = encrypt(item[1]);
//            System.out.println(addr1 + "," + addr2 + "\n");
            writer.write(addr1 + "," + addr2 + "\n");
        }
        checkReader.close();
        writer.flush();
        writer.close();
    }

    /**
     * 对hash地址简单加密，字符后移一位，并删除前5位
     * @param s
     * @return
     */
    private static String encrypt(String s) {
        int newLen = s.length() - 5;
        char[] newChars = new char[newLen];
        for (int i = 0; i < newLen; i ++) {
            int currentNumber = Integer.valueOf(s.charAt(i + 5));
            int newNumber = currentNumber + 1;
            if (newNumber == 58) {
                newNumber = 48;
            }
            if (newNumber == 91) {
                newNumber = 65;
            }
            if (newNumber == 123) {
                newNumber = 97;
            }
            newChars[i] = (char) newNumber;
        }
        return new String(newChars);
    }
}
