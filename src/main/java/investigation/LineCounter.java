package investigation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/***
 * Application20211029 生成结果 行数统计
 * @author xuda
 */
public class LineCounter {
    public static void main(String[] args) throws IOException {
//        BufferedReader checkReader = new BufferedReader(new FileReader("/usr/dxu/java-app/ld/summary2.csv"));
        BufferedReader checkReader = new BufferedReader(new FileReader(args[0]));
        String checkTitleLine = checkReader.readLine();
        String[] checkColumnName = checkTitleLine.split(",", -1);
        int checkColumnCount = checkColumnName.length;
        System.out.println("title column count: " + checkColumnCount);
        String checkLine = null;
//        int xnCount = 0;
        int validCount = 0;
        while ((checkLine = checkReader.readLine()) != null) {
//            String item[] = checkLine.split(",", -1);//不忽略空值
//            System.out.println("item.length: " + item.length);
//            if (item.length == checkColumnCount) {
//                if (item[0].startsWith("XN_")) {
//                    xnCount ++;
//                } else {
//                    validCount++;
//                }
//            } else {
//                System.out.println("异常 summary line: " + checkLine);
//            }
            validCount ++;
        }
//        System.out.println("xnCount: " + xnCount);
        System.out.println("file line count: " + validCount);
        checkReader.close();
    }
}
