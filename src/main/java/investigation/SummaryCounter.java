package investigation;

import java.io.*;

/**
 * Application20211029 生成结果 行数统计
 * @author xuda
 */
public class SummaryCounter {
    public static void main(String[] args) throws IOException {
        BufferedReader checkReader = new BufferedReader(new FileReader("/Users/xuda/Documents/summary2_sample_head.csv"));
        String checkTitleLine = checkReader.readLine();
        String[] checkColumnName = checkTitleLine.split(",", -1);
        int checkColumnCount = checkColumnName.length;
        System.out.println("checkColumnCount: " + checkColumnCount);
        String checkLine = null;
        int xnCount = 0;
        int validCount = 0;
        while ((checkLine = checkReader.readLine()) != null) {
            String item[] = checkLine.split(",", -1);
            System.out.println("item.length: " + item.length);
            if (item.length == checkColumnCount) {
                if (item[0].startsWith("XN_")) {
                    xnCount ++;
                } else {
                    validCount++;
                }
            } else {
                System.out.println("异常 summary line: " + checkLine);
            }
        }
        System.out.println("xnCount: " + xnCount);
        System.out.println("validCount: " + validCount);
        checkReader.close();
    }
}
