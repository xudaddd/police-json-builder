package model;

import java.util.*;

/**
 * 一般图
 * @author xuda
 */

public class Graph {
    /**
     * 图名称
     */
    protected String name;

    /**
     * 节点入度表
     */
    protected Map<Integer, Integer> inDegree = new HashMap<>();

    /**
     * 节点出度表
     */
    protected Map<Integer, Integer> outDegree = new HashMap<>();

    /**
     * 边集 保存原始数据
     */
    protected Set<SimpleEdge> edgeSet = new HashSet<>();

    /**
     * 存储原始文件中显示给出的顶层节点信息
     * 有可能不止一个，可能是森林的根，也可能是树根
     * 注意这个集合并不是所有顶层节点信息，只包含在原始文件中有属性条目列出的。因为有的节点在结构上作为根出现，不过并没有在原始文件中单独列出属性。
     */
    protected Map<String, NodeProperties> topNodeInfo = new HashMap<>();

    /**
     * 图中所有的边集，使用边的起点做key
     * 通常是 <>edgeSet</> 进一步加工而来
     */
    protected Map<Integer, List<SimpleEdge>> edgeCollection = new HashMap<>();



    /**
     * 利用边信息初始化出入度信息，Graph 和 TreeGraph 都适用
     * 使用条件：inDegree, outDegree, edgeSet 初始化完整
     */
    public void selfComputeInOutDegree() {
        if (inDegree == null || inDegree.size() == 0) {
            System.out.println("Graph.inDegree 未适当初始化");
            return;
        }
        if (outDegree == null || outDegree.size() == 0) {
            System.out.println("Graph.outDegree 未适当初始化");
            return;
        }
        if (edgeSet == null || edgeSet.size() == 0) {
            System.out.println("Graph.edgeSet 未适当初始化");
            return;
        }
        try {
            for (SimpleEdge e : edgeSet) {
                int oldOutDegree = outDegree.get(e.getSource()) + 1;
                int oldInDegree = inDegree.get(e.getTarget()) + 1;
                outDegree.put(e.getSource(), oldOutDegree);
                inDegree.put(e.getTarget(), oldInDegree);
                if (!edgeCollection.containsKey(e.getSource())) {
                    edgeCollection.put(e.getSource(), new ArrayList<>());
                }
                edgeCollection.get(e.getSource()).add(e);
            }
        } catch (NullPointerException e) {
            System.out.println("Graph.inDegree, Graph.outDegree, Graph.edgeSet 数据可能不完整");
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Integer, Integer> getInDegree() {
        return inDegree;
    }

    public void setInDegree(Map<Integer, Integer> inDegree) {
        this.inDegree = inDegree;
    }

    public Map<Integer, Integer> getOutDegree() {
        return outDegree;
    }

    public void setOutDegree(Map<Integer, Integer> outDegree) {
        this.outDegree = outDegree;
    }

    public Set<SimpleEdge> getEdgeSet() {
        return edgeSet;
    }

    public void setEdgeSet(Set<SimpleEdge> edgeSet) {
        this.edgeSet = edgeSet;
    }


}