package model;

import lombok.Data;

/**
 * @author xuda
 */
@Data
public class JsonEdge {

    private String source;
    private String target;
    private String label;

    public JsonEdge(Integer source, Integer target){
        this.source = String.valueOf(source);
        this.target = String.valueOf(target);
    }
}
