package model;

import lombok.Data;

/**
 * @author xuda
 */
@Data
public class JsonNode {

    private String id;
    private String label;

    public JsonNode(Integer idParam){
        id = String.valueOf(idParam);
        label = String.valueOf(idParam);
    }

    public JsonNode(SimpleNode simpleNode){
        id = simpleNode.getId();
        label = id;
    }
}
