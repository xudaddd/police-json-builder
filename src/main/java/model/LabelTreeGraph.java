package model;

import constant.GraphVirtualConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 带节点标识（label）的树图
 *  有的原始文件中不用数字id代表节点，而用地址hash或者其他字符串，为了计算方便，需要我们自己给节点编号，生成id-label映射表
 * @author xuda
 * @date 2022/7/12
 */
public class LabelTreeGraph extends TreeGraph{

    /**
     * 增加id和label的映射表
     * 当图中存在该属性时，节点除了用id表示，还需要有label
     */
    protected Map<Integer, String> idLabelMap;

    /**
     * 顶层节点标识列序号
     * 主要用在：原始文件中未列出顶层节点（根节点），不过提供了idLabelMap对应表
     */
    protected int topLevelNodeLabelColumnIndex = 2;

    /**
     * selfComputeInOutDegree 之后，selfBuildTree 之前，把森林变成树
     * 代替 detectRoot 和 setRoot（withLabel）
     * 尽量不再修改原始文件
     *
     */
    @Override
    public void makeBundle() {
        List<String> qualifiedRoots = findRoots();
        if (qualifiedRoots.size() > 1) {
            // 有多个根，检测到森林
            // 严格来说检测到多个根并不能说明一定是森林，也有可能是DAG，如果 检测到的根数量 + 边数量 = 点数量 才是森林；不过对于传销案件来说，一个人只有一个推荐人（parentId 只有一个）
            System.out.println(qualifiedRoots.size() + " qualified roots found, this MAY be a forest");
            TreeNode virtualRoot = new TreeNode(GraphVirtualConstants.VIRTUAL_ROOT_ID);
            // 虚拟根没有属性，需要新建
            NodeProperties nodeProperties = new NodeProperties();
            nodeProperties.saveProperty(columnTitleNames[idColumnIndex], GraphVirtualConstants.VIRTUAL_ROOT_ID);
            virtualRoot.setNodeProperties(nodeProperties);
            virtualRoot.setLabel(GraphVirtualConstants.VIRTUAL_ROOT_LABEL);
            this.inDegree.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), 0);
            this.outDegree.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), qualifiedRoots.size());
            List<SimpleEdge> collection = new ArrayList<>();
            for (String s : qualifiedRoots) {
                SimpleEdge simpleEdge;
                if (getTopNodeInfo().containsKey(s)) {
                    // 原始文件中显示列出了顶层节点（根节点）
                    simpleEdge = new SimpleEdge(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), Integer.parseInt(s), getTopNodeInfo().get(s));
                } else {
                    // 原始文件中未列出顶层节点（根节点）
                    NodeProperties properties = new NodeProperties();
                    // 此时顶层节点的 properties 我们并不知道，如有必要，需要特别指定
                    properties.getPropertyMap().put(columnTitleNames[parentIdColumnIndex], GraphVirtualConstants.VIRTUAL_ROOT_ID);
                    // 如果提供了 idLabelMap，那label(多半是地址hash)可能是能提供的唯一属性信息
                    properties.getPropertyMap().put(columnTitleNames[topLevelNodeLabelColumnIndex], this.idLabelMap.get(Integer.parseInt(s)));
                    simpleEdge = new SimpleEdge(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), Integer.parseInt(s), properties);
                }
                this.edgeSet.add(simpleEdge);
                collection.add(simpleEdge);
            }
            this.edgeCollection.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), collection);
            this.idLabelMap.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), GraphVirtualConstants.VIRTUAL_ROOT_LABEL);
            this.getRoots().add(virtualRoot);
        } else if (qualifiedRoots.size() == 1) {
            // 只有一个根，检测到树
            System.out.println("1 qualified root found, this MAY be a tree");
            // 为这个树根添加properties
            if (topNodeInfo.isEmpty()) {
                // topNodeInfo 为空，原始文件中没有显示提供根信息
                this.setRootWithLabel(qualifiedRoots.get(0), new NodeProperties());
            } else {
                // topNodeInfo 不为空，原始文件中显示地提供了根信息
                this.setRootWithLabel(qualifiedRoots.get(0), topNodeInfo.get(qualifiedRoots.get(0)));
            }
        }
    }

    @Override
    public void detectRoot() {
        for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value.intValue() == 0 && this.outDegree.get(key).intValue() >= 0) {
                // 根节点入度为0，出度大于等于0
                System.out.println(name + " root found: " + key + " " + idLabelMap.get(key) + " in/out: " + value + "/" + this.outDegree.get(key).intValue());
            }
        }
    }

    /**
     * 新的获得叶子节点方法，尝试加快速度
     * @param id
     * @param currentLevel
     * @param isCollapsed
     * @return
     */
    @Override
    public List<TreeNode> getLeafsFaster(Integer id, int currentLevel, boolean isCollapsed) {
        List<TreeNode> l = new ArrayList<TreeNode>();
        List<SimpleEdge> finOut = edgeCollection.get(id);
        for (SimpleEdge e: finOut) {
            l.add(new TreeNode(e.getTarget(), currentLevel + 1, isCollapsed, e.getNodeProperties(), String.valueOf(id), idLabelMap.get(e.getTarget())));
        }
        return l;
    }

    /**
     * 按照 节点id 手动设定图的根
     * 设置根的同时，添加label，前提是设定了id-label map
     * 为根添加指定 properties
     * @param id
     * @param properties
     */
    @Override
    public void setRootWithLabel(String id, NodeProperties properties) {
        roots.add(new TreeNode(id, idLabelMap.get(Integer.parseInt(id)),0,true, properties));
    }

    public void setTopLevelNodeLabelColumnIndex(int i) {
        this.topLevelNodeLabelColumnIndex = i;
    }

    public int getTopLevelNodeLabelColumnIndex() {
        return topLevelNodeLabelColumnIndex;
    }

    public Map<Integer, String> getIdLabelMap() {
        return idLabelMap;
    }

    public void setIdLabelMap(Map<Integer, String> idLabelMap) {
        this.idLabelMap = idLabelMap;
    }
}
