package model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;


/**
 * 用来存储树节点属性信息
 * @author xuda
 */
@Data
public class NodeProperties {

    private Map<String, String> propertyMap = new HashMap<>();

    public void saveProperty(String key, String value){
        propertyMap.put(key, value);
    }

    public String[] getPropertyKeys() {
        if (propertyMap != null) {
            return propertyMap.keySet().toArray(new String[0]);
        } else {
            return null;
        }
    }
}
