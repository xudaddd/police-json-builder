package model;

import lombok.Data;

/**
 * 用来存储边和source节点的属性
 * @author xuda
 */
@Data
public class SimpleEdge {

    protected Integer source;
    protected Integer target;

    /**
     * 目标节点属性信息
     * 把目标节点属性信息放到边里有点儿奇怪，主要为了方便逐行提取属性信息
     */
    protected NodeProperties nodeProperties;

    public SimpleEdge(Integer source, Integer target) {
        this.source = source;
        this.target = target;
    }

    public SimpleEdge(Integer source, Integer target, NodeProperties nodeProperties) {
        this.source = source;
        this.target = target;
        this.nodeProperties = nodeProperties;
    }
}
