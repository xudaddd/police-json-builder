package model;

import lombok.Data;

/**
 * 简单节点
 * @author xuda
 */
@Data
public class SimpleNode {
    protected String id;
}
