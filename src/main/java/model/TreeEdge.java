package model;

import lombok.Data;

import java.util.Objects;


/***
 * 可以根据 source & target 区别去重的边
 * @author xuda
 */
@Data
public class TreeEdge {
    private String source;
    private String target;
    public TreeEdge(String source, String target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreeEdge that = (TreeEdge) o;
        if (Objects.equals(source, that.source) && Objects.equals(target, that.target)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(source + target);
    }
}
