package model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import constant.GraphVirtualConstants;
import custom.EffectiveNodeFilter;
import model.temp.ReportItem;
import model.temp.UserInfoMarch;
import model.temp.UserName;

import java.io.*;
import java.util.*;


/**
 * 树
 * 主要用途：构建&存储传销层级树
 * @author xuda
 *
 *
 *
 *
 *
 * 注意：当树中节点出现了 effective 的区别之后，统计 effectiveBranchCount 和 effectiveBranchHeight 的方法也会随着变化：
 *  effectiveBranchCount 比较简单，只要递归遍历即可 （已实现）
 *  effectiveBranchHeight 比较复杂，目前的方法是计算过程的最后，不断循环遍历 traversal 结构，删除 ineffective 的叶子（可能既需要删除叶子，也需要删除指向叶子的"指针" 未实现）
 *
 *
 */

public class TreeGraph extends Graph {

    /**
     * 层序信息
     * key是层号，value是id集合
     */
    protected Map<Integer, Set<Integer>> levelSet = new HashMap<>();

    /**
     * 树根列表，对于正常传销层级来说，只有一个根
     */
    protected List<TreeNode> roots = new ArrayList<>();

    /**
     * 树中所有节点的遍历
     */
    protected Map<Integer, TreeNode> traversal = new HashMap<>();

    /**
     * 伞下人数 按id排序，通常从高到底取若干
     */
    protected List<Integer> topBranchCount = new ArrayList<>();

    /**
     * 伞高 按id排序，通常从高到低取若干
     */
    protected List<Integer> topBranchHeight = new ArrayList<>();

    /**
     * 直接下级 按id排序，通常从高到低取若干
     */
    protected List<Integer> topChildrenCount = new ArrayList<>();

    /**
     * 原始文件属性列名数组
     */
    protected String[] columnTitleNames = null;

    /**
     * "id" 列 顺序号
     * 默认为第一列
     */
    protected int idColumnIndex = 0;

    /**
     * "parentId"列 顺序号
     * 默认为第二列
     */
    protected int parentIdColumnIndex = 1;

    public void setColumnTitleNames(String[] columnNames) {
        this.columnTitleNames = columnNames;
    }

    /**
     * 检测图中的环
     * 遍历 edgeCollection，每个节点作为入口
     */
    public void loopDetect() {
        System.out.println("detecting loop...");
        Map<Integer, Boolean> checkMap = Collections.synchronizedMap(new HashMap<>());
        edgeCollection.entrySet().parallelStream().forEach( entry -> {
            Integer nodeId = entry.getKey();
            List<Integer> ancestors = new ArrayList<>();
            checkMap.put(nodeId, dive(nodeId, ancestors));
        });
        for (Map.Entry<Integer, Boolean> entry : checkMap.entrySet()) {
            if (!entry.getValue()) {
                System.out.println("loops exist, the graph is NOT OK");
                return;
            }
        }
        System.out.println("no loops, the graph is OK");
    }

    /**
     * 检测节点以下是否存在圈
     * @param nodeId 目标节点id
     * @param ancestors 目标节点的祖先id列表
     * @return
     */
    private Boolean dive(Integer nodeId, List<Integer> ancestors) {
        List<SimpleEdge> edges = this.edgeCollection.get(nodeId);
        ancestors.add(nodeId);
        if (edges != null && !edges.isEmpty()) {
            // 节点所有的后代都没有圈，则认为该节点没有圈
            Set<Boolean> set = new HashSet<>();
            for (SimpleEdge edge : edges) {
                Integer out = edge.getTarget();
                if (!ancestors.contains(out)) {
                    // 注意每次递归使用独立祖先id列表
                    List<Integer> myAncestors = new ArrayList<>(ancestors);
                    Collections.copy(myAncestors, ancestors);
                    set.add(dive(out, myAncestors));
                } else {
                    printLoop(out, ancestors);
                    set.add(false);
                }
            }
            return !set.contains(false);
        }
        return true;
    }

    /**
     * 根据节点id和祖先id列表打印圈
     * @param node 节点id
     * @param ancestors 节点祖先id列表
     * @return
     */
    private void printLoop(Integer node, List<Integer> ancestors) {
        List<Integer> loop = new ArrayList<>();
        loop.addAll(ancestors);
        loop.add(node);
        System.out.println("loop detected: " + StringUtils.join(loop, "->"));
    }


    /**
     * 如果有的节点入度大于1，说明它有两个父亲
     */
    public void multiParentsDetect() {
        Set<Integer> multiSet = Collections.synchronizedSet(new HashSet<>());
        inDegree.entrySet().parallelStream().forEach( entry -> {
            if (entry.getValue() > 1) {
                multiSet.add(entry.getKey());
            }
        });
        if (multiSet.isEmpty()) {
            System.out.println("no multi parent, the graph is OK");
        } else {
            System.out.println("multi parent exists: " + multiSet);
        }
    }


    /**
     * 获得目标节点的所有子孙（包括自己）
     * @param root
     * @return
     */
    public List<TreeNode> getDescendantsList(TreeNode root){
        List<TreeNode> result = new ArrayList<>();
        scanDescendants(root, true, 0, result);
        return result;
    }

    /**
     * 递归获得所有子孙列表
     * @param root
     * @param isCollapsed
     * @param levelLimit
     * @param descendants 收集到的子孙节点
     */
    private void scanDescendants(TreeNode root, boolean isCollapsed, int levelLimit, List<TreeNode> descendants) {
        descendants.add(root);
        if (isLeaf(Integer.parseInt(root.getId()))) {
            // 叶子节点永远展开
            root.setCollapsed(false);
        } else {
            if (levelLimit == 0) {
                for (TreeNode branch: root.getChildren()) {
                    scanDescendants(branch, isCollapsed, levelLimit, descendants);
                }
            } else if (root.getSubLevel() < levelLimit) {// copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    scanDescendants(branch, isCollapsed, levelLimit, descendants);
                }
            } else if (root.getSubLevel() >= levelLimit) { //切断 children 层级引用 copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    branch.setChildren(null);
                }
            }
        }
    }

    /**
     * 寻找最近公共祖先
     * @return 公共祖先id
     */
    public Integer findLowestCommonAncestor(List<Integer> ids) {
        if (ids != null && ids.size() > 1) {
            Map<Integer, List<Integer>> comb = new HashMap<>();
            for (Integer i : ids) {
                //有的节点id获取不到路径
                List<Integer> path = getPathFromRoot(i);
                if (path != null && path.size() > 0) {
                    comb.put(i, path);
                } else {
                    System.out.println("cant's get path for: " + i);
                }
            }
            //找到最短的路径长度和路径id，从ids第一个开始
            int shortest = comb.get(ids.get(0)).size();
            int shortestId = ids.get(0);
            for (Map.Entry<Integer, List<Integer>> entry : comb.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue().toString());
                if (entry.getValue().size() <= shortest) {
                    shortest = entry.getValue().size();
                    shortestId = entry.getKey();
                }
            }
            //do the comb
            Integer ancestorPos = 0;
            for (int pos = 0; pos < shortest; pos ++) {
                int ref = comb.get(shortestId).get(pos);
                boolean diff = false;
                for (Map.Entry<Integer, List<Integer>> entry : comb.entrySet()) {
                    if (entry.getValue().get(pos) != ref) {
                        diff = true;
                        break;
                    }
                }
                if (diff) {
                    ancestorPos = pos - 1;
                    break;
                } else {
                    ancestorPos = pos;
                }
            }
            //任意一条路径上ancestorPos位置的值就是寻找最近公共祖先
            return comb.get(ids.get(0)).get(ancestorPos);
        }
        //如果找不到，或者目标点少于两个就返回默认根的id
        return 0;
    }


    /**
     * 对图中节点按关键字搜索（单一属性）
     * 单纯遍历
     * @return
     */
    public List<TreeNode> simpleSearchNodesOnProperty(String property, String word) {
        List<TreeNode> result = new ArrayList<>();
        for (Map.Entry<Integer, TreeNode> entry : this.traversal.entrySet()) {
            NodeProperties properties = entry.getValue().getNodeProperties();
            if (properties != null) {
                String candidate = properties.getPropertyMap().get(property);
                if (candidate != null && candidate.contains(word)) {
                    result.add(entry.getValue());
                }
            } else {
                System.out.println(entry.getKey() + " has no properties");
            }
        }
        return result;
    }

    /**
     * 对图中节点按关键字搜索（多属性）
     * 单纯遍历
     * @return
     */
    public List<TreeNode> simpleSearchNodesOnProperties(List<String> propertyList, String word) {
        List<TreeNode> result = new ArrayList<>();
        for (Map.Entry<Integer, TreeNode> entry : this.traversal.entrySet()) {
            NodeProperties properties = entry.getValue().getNodeProperties();
            if (properties != null) {
                int count = propertyList.size();
                for (int i = 0; i < count; i ++) {
                    String candidate = properties.getPropertyMap().get(propertyList.get(i));
                    if (candidate != null && candidate.contains(word)) {
                        result.add(entry.getValue());
                        break;
                    }
                }
            } else {
                System.out.println(entry.getKey() + " has no properties");
            }
        }
        return result;
    }

    /**
     * 对图中节点按关键字搜索（全部属性）
     * 单纯遍历
     * @return
     */
    public List<TreeNode> simpleSearchNodesOnAllProperties(String word) {
        List<TreeNode> result = new ArrayList<>();
        for (Map.Entry<Integer, TreeNode> entry : this.traversal.entrySet()) {
            NodeProperties properties = entry.getValue().getNodeProperties();
            if (properties != null) {
                for (Map.Entry<String, String> localEntry : properties.getPropertyMap().entrySet()) {
                    if (localEntry.getValue() != null && localEntry.getValue().contains(word)) {
                        result.add(entry.getValue());
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 打印最长路径（其中一条）及其直接下级人数
     * @param path 文件输出路径
     */
    public void printSomeLongestPath(String path) {
        BufferedWriter txtWriter = null;
        //首先到level最大的节点
        TreeNode maxLevelNode = new TreeNode(0, 0, false);
        for (Map.Entry<Integer, TreeNode> entry : traversal.entrySet()) {
            if (entry.getValue().getLevel() >= maxLevelNode.getLevel()) {
                maxLevelNode = entry.getValue();
            }
        }
        System.out.println("longest path ending at TreeNode: " + maxLevelNode.getId() + "("+maxLevelNode.getLevel()+")");
        List<Integer> longestPath = getPathFromRoot(Integer.parseInt(maxLevelNode.getId()));
        try {
            txtWriter = new BufferedWriter(new FileWriter(path));
            for (Integer i : longestPath) {
                TreeNode node = traversal.get(i);
//                String line = idLabelMap.get(Integer.parseInt(node.getId())) + "  直接下级人数/所在层级总人数： " + node.getChildrenCount() + " / "+levelSet.get(node.getLevel()).size()+"\n";
                String line = node.getId() + "  直接下级人数/所在层级总人数： " + node.getChildrenCount() + " / "+levelSet.get(node.getLevel()).size()+"\n";
                txtWriter.write(line);
            }
            txtWriter.flush();
            txtWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * 单纯获取子树
     * @param nodeId
     * @param isCollapsed
     * @param extendLimit
     * @return
     */
    public TreeNode getSubTree(Integer nodeId, boolean isCollapsed, int extendLimit) {
        TreeNode starter = new TreeNode(nodeId);
        TreeNode shadow = traversal.get(nodeId);
//        starter.setId(shadow.getId());
        starter.setBranchCount(shadow.getBranchCount());
        starter.setBranchHeight(shadow.getBranchHeight());
        starter.setChildren(shadow.getChildren());
        starter.setCollapsed(shadow.isCollapsed());
        starter.setLevel(shadow.getLevel());
        starter.setSubLevel(0);
        starter.setParentId(shadow.getParentId());
        starter.setEffectiveBranchCount(shadow.getEffectiveBranchCount());
        // 拷贝属性信息
        starter.setNodeProperties(shadow.getNodeProperties());
        if (starter == null) {
            System.out.println("未找到节点");
            return null;
        } else {
            grow(starter, isCollapsed, extendLimit);
            return starter;
        }
    }

    /**
     * 以某一个节点为根，获取若干层子树，转换成json
     * @param nodeId
     * @param extendLimit
     */
    public TreeNode getSubTree(Integer nodeId, boolean isCollapsed, int extendLimit, Map<Integer, UserName> userNameMap, Map<Integer, UserInfoMarch> userInfoMarchMap, Map<Integer, ReportItem> reportMap) {
        TreeNode starter = new TreeNode(nodeId);
        TreeNode shadow = traversal.get(nodeId);
        starter.setBranchCount(shadow.getBranchCount());
        starter.setBranchHeight(shadow.getBranchHeight());
        starter.setChildren(shadow.getChildren());
        starter.setCollapsed(shadow.isCollapsed());
        starter.setLevel(shadow.getLevel());
        starter.setSubLevel(0);
        starter.setParentId(shadow.getParentId());
        if (starter == null) {
            System.out.println("未找到节点");
            return null;
        } else {
            System.out.println("开始构建 "+ nodeId+" 的子树");
            System.out.println(" " + starter.getBranchCount() + " " + starter.getBranchHeight() + " " + starter.isCollapsed() + " " + starter.getLevel() + " " + starter.getSubLevel() + " " + starter.getChildren().size());
            growAndOutput(starter, isCollapsed, extendLimit, userNameMap, userInfoMarchMap, reportMap);
            return starter;
        }
    }

    /**
     * todo 怀疑有bug 递归时应该用 root.getSubLevel() 而不是 root.getLevel() 参考 PoliceLD TreeGraph.java 345行
     *
     * 以 root 为根，从已经计算好的图里 单纯 拷贝 levelLimit 层
     * @param root
     * @param isCollapsed
     * @param levelLimit 0 表示不限制
     */
    public void grow(TreeNode root, boolean isCollapsed, int levelLimit) {
        // children[] 空判断
        if (isLeaf(Integer.parseInt(root.getId()))) {
            // 叶子节点永远展开
            root.setCollapsed(false);
        } else {
            List<TreeNode> newChildren = new ArrayList<TreeNode>();
            for (TreeNode child: root.getChildren()) {
                TreeNode newChild = new TreeNode(child.getId());
                // 拷贝&设置结构信息
                newChild.setBranchCount(child.getBranchCount());
                newChild.setBranchHeight(child.getBranchHeight());
                newChild.setChildren(child.getChildren());
                newChild.setCollapsed(child.isCollapsed());
                newChild.setLevel(root.getLevel() + 1);//todo 貌似应该用 child.getLevel()
                newChild.setSubLevel(root.getSubLevel() + 1);
                newChild.setParentId(root.getId());
                // 拷贝属性信息
                newChild.setNodeProperties(child.getNodeProperties());
                newChildren.add(newChild);
            }
            root.setChildren(newChildren);
            if (levelLimit == 0) {
                for (TreeNode branch: root.getChildren()) {
                    grow(branch, isCollapsed, levelLimit);
                }
            } else if(root.getLevel() < levelLimit) {
                for (TreeNode branch: root.getChildren()) {
                    grow(branch, isCollapsed, levelLimit);
                }
            } else if (root.getLevel() >= levelLimit) {// 切断 children 层级引用
                for (TreeNode branch: root.getChildren()) {
                    branch.setChildren(null);
                }
            }
        }
    }

    /**
     * 响应6月7日案件
     * @deprecated
     * @param root
     * @param isCollapsed
     * @param levelLimit
     */
    public void growAndOutput(TreeNode root, boolean isCollapsed, int levelLimit, Map<Integer, UserName> userNameMap, Map<Integer, UserInfoMarch> userInfoMarchMap, Map<Integer, ReportItem> reportMap) {
        Integer id = Integer.parseInt(root.getId());
        ReportItem item = new ReportItem();
        if (userInfoMarchMap.containsKey(id)) {
            item.setProperties(userInfoMarchMap.get(id));
        }
        item.setPath(getPathStringFromRoot(id));
        if (userNameMap.containsKey(id)) {
            item.setReal_name(userNameMap.get(id).getReal_name());
            item.setId_number(userNameMap.get(id).getId_number());
        }
        item.setParent_id(root.getParentId());
        item.setSub_level(root.getSubLevel());
        item.setLevel(root.getLevel());
        reportMap.put(id, item);

        if (isLeaf(Integer.parseInt(root.getId()))) {
            // 叶子节点永远展开
            root.setCollapsed(false);
        } else {
            List<TreeNode> newChildren = new ArrayList<>();
            for (TreeNode child: root.getChildren()) {
                TreeNode newChild = new TreeNode(child.getId());
                newChild.setBranchCount(child.getBranchCount());
                newChild.setBranchHeight(child.getBranchHeight());
                newChild.setChildren(child.getChildren());
                newChild.setCollapsed(child.isCollapsed());
                newChild.setLevel(child.getLevel());
                newChild.setSubLevel(root.getSubLevel() + 1);
                newChild.setParentId(child.getParentId());
                newChildren.add(newChild);
            }
            root.setChildren(newChildren);
            if (levelLimit == 0) {
                for (TreeNode branch: root.getChildren()) {
                    growAndOutput(branch, isCollapsed, levelLimit, userNameMap, userInfoMarchMap, reportMap);
                }
            } else if (root.getLevel() < levelLimit) {// copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    growAndOutput(branch, isCollapsed, levelLimit, userNameMap, userInfoMarchMap, reportMap);
                }
            } else if (root.getLevel() >= levelLimit) {//切断 children 层级引用 copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    branch.setChildren(null);
                }
            }
        }
    }

    /**
     * 获取根到节点的路径(id,id,id,id,...id,)
     * @param id 节点id
     * @return 路径的字符串表示，用逗号相连
     */
    public String getPathStringFromRoot(Integer id) {
        TreeNode currentNode = traversal.get(id);
        Stack<Integer> stack = new Stack<>();
        // 存在parent
        while (currentNode.getParentId() != null && inDegree.get(id).intValue() > 0) {
            Integer parentId = Integer.parseInt(currentNode.getParentId());
            stack.push(parentId);
            currentNode = traversal.get(parentId);
        }
        if (!stack.empty()) {
            StringBuilder path = new StringBuilder("");
            while (!stack.empty()) {
                String seg = stack.pop() + ", ";
                path = path.append(seg);
            }
            return path.toString();
        } else {
            System.out.println("no path found for TreeNode:" + id);
            return "";
        }
    }

    /**
     * 获取根到节点的路径，包括节点自己id
     * @param id 节点id
     * @return 路径id列表
     *
     */
    public List<Integer> getPathFromRoot(Integer id) {
        TreeNode currentNode = traversal.get(id);
        Stack<Integer> stack = new Stack<>();
        List<Integer> result = new ArrayList<>();
        // id存在，id存在parent
        while (currentNode != null && currentNode.getParentId() != null && inDegree.get(id).intValue() > 0) {
            Integer parentId = Integer.parseInt(currentNode.getParentId());
            stack.push(parentId);
            currentNode = traversal.get(parentId);
        }
        if (!stack.empty()) {
            StringBuilder path = new StringBuilder("");
            while (!stack.empty()) {
//                String seg = stack.pop() + ", ";
//                path = path.append(seg);
                result.add(stack.pop());
            }
            //补全自己
            result.add(id);
            return result;
        } else {
            System.out.println("no path found for TreeNode:" + id);
            return null;
        }
    }


    /**
     * 8月16日添加
     * 获取根到节点的路径
     * @param id 节点id
     * @param propertyName 目标属性名
     * @return 不使用 id，而是使用某一个属性来显示路径(<propertyName>, <propertyName>, <propertyName>, <propertyName>,...<propertyName>,)
     */
    public String getPathStringFromRoot(Integer id, String propertyName) {
        TreeNode currentNode = traversal.get(id);
        Stack<Integer> stack = new Stack<>();
        // 存在parent
        while (currentNode.getParentId() != null && inDegree.get(id).intValue() > 0) {
            Integer parentId = Integer.parseInt(currentNode.getParentId());
            stack.push(parentId);
            currentNode = traversal.get(parentId);
        }
        if (!stack.empty()) {
            StringBuilder path = new StringBuilder("");
            while (!stack.empty()) {
                int pop = stack.pop();
                if (traversal.get(pop).getNodeProperties() != null) {
                    String seg = traversal.get(pop).getNodeProperties().getPropertyMap().get(propertyName) + ", ";
                    path = path.append(seg);
                }
            }
            return path.toString();
        } else {
            System.out.println("no path found for TreeNode:" + id);
            return "";
        }
    }

    /**
     * 11月1日添加
     * 获取根到节点的路径
     * @param id 节点id
     *      * @param linkSymbol 路径连接符
     * @return 使用 id 来显示路径(<id>, <id>, <id>, <id>,...<id>,)
     */
    public String getPathStringFromRootById(Integer id, String linkSymbol) {
        TreeNode currentNode = traversal.get(id);
        Stack<Integer> stack = new Stack<>();
        // 存在parent
        while (currentNode.getParentId() != null && inDegree.get(id).intValue() >= 0) { //为了显示虚拟根节点，条件变成 >=
            Integer parentId = Integer.parseInt(currentNode.getParentId());
            stack.push(parentId);
            currentNode = traversal.get(parentId);
        }
        if (!stack.empty()) {
            StringBuilder path = new StringBuilder("");
            while (!stack.empty()) {
                int pop = stack.pop();
                String seg = traversal.get(pop).getId() + linkSymbol;
                path = path.append(seg);

            }
            return path.toString();
        } else {
            System.out.println("no path found for TreeNode:" + id);
            return "";
        }
    }

    /**
     * 11月1日添加
     * 获取根到节点的路径
     * @param id 节点id
     * @param linkSymbol 路径连接符
     * @return 不使用 id，而是使用某一个属性来显示路径(<propertyName>, <propertyName>, <propertyName>, <propertyName>,...<propertyName>,)
     */
    public String getPathStringFromRoot(Integer id, String propertyName, String linkSymbol) {
        TreeNode currentNode = traversal.get(id);
        Stack<Integer> stack = new Stack<>();
        // 存在parent
        while (currentNode.getParentId() != null && inDegree.get(id).intValue() > 0) {
            Integer parentId = Integer.parseInt(currentNode.getParentId());
            stack.push(parentId);
            currentNode = traversal.get(parentId);
        }
        if (!stack.empty()) {
            StringBuilder path = new StringBuilder("");
            while (!stack.empty()) {
                int pop = stack.pop();
                if (traversal.get(pop).getNodeProperties() != null) {
                    String seg = traversal.get(pop).getNodeProperties().getPropertyMap().get(propertyName) + linkSymbol;
                    path = path.append(seg);
                }
            }
            return path.toString();
        } else {
            System.out.println("no path found for TreeNode:" + id);
            return "";
        }
    }

    /**
     * 从指定根开始递归构建树图
     * @param rootIndex 根的序号，通常就是第一个
     * @param isCollapsed 是否收起
     * @param levelLimit 如果为0，则不限制层数
     */
    public void selfBuildTree(int rootIndex, boolean isCollapsed, int levelLimit) {
        if (this.roots == null || this.roots.size() == 0) {
            System.out.println("TreeGraph.roots 未被初始化 无法建树");
            return;
        }
        buildTree(roots.get(rootIndex), isCollapsed, levelLimit);
    }

    /**
     * 递归构建以root为根的树图，并记录层序信息
     * @param root
     * @param isCollapsed 是否收起
     * @param levelLimit 为0，则不限get制层数
     */
    public void buildTree(TreeNode root, boolean isCollapsed, int levelLimit) {
        if(levelSet.get(root.getLevel()) == null){
            levelSet.put(root.getLevel(),new HashSet<Integer>());
        }
        levelSet.get(root.getLevel()).add(Integer.parseInt(root.getId()));
        if (isLeaf(Integer.parseInt(root.getId()))) {
            root.setCollapsed(false);
        } else {
            root.setChildren(getLeafsFaster(Integer.parseInt(root.getId()), root.getLevel(), isCollapsed));
            if (levelLimit == 0) {
                for(TreeNode branch: root.getChildren()){
                    buildTree(branch, isCollapsed, levelLimit);
                }
            } else if (root.getLevel() < levelLimit) {
                for (TreeNode branch: root.getChildren()) {
                    buildTree(branch, isCollapsed, levelLimit);
                }
            }
        }
    }

    /**
     * 遍历树，并统计：
     *      所有子孙人数；
     *      一级子孙人数；
     *      下线层数；
     * 必须在 buildTree 调用之后使用
     * @param root 起始节点
     */
    public void traverseTree(TreeNode root) {
        if (root.getChildren() == null || root.getChildren().size() == 0) {
            // 叶子节点子树节点数为1
            root.setBranchCount(1);
            // 叶子节点子树高度为0
            root.setBranchHeight(0);
        } else {
            for (TreeNode branch: root.getChildren()) {
                traverseTree(branch);
            }
            int currentBranchCount = 0;
            int currentBranchHeight = 0;
            for (TreeNode branch: root.getChildren()) {
                currentBranchCount += branch.getBranchCount();
                if (branch.getBranchHeight() > currentBranchHeight) {//找到孩子节点高度最高的
                    currentBranchHeight = branch.getBranchHeight();
                }
            }
            // 子树节点数 +1
            root.setBranchCount(currentBranchCount + 1);
            // 子树高度 +1
            root.setBranchHeight(currentBranchHeight + 1);
        }
        traversal.put(Integer.parseInt(root.getId()),root);
    }

    /**
     * 响应 10月29日 慧文案件
     * 遍历树，并统计：
     *      所有子孙人数；
     *      一级子孙人数；
     *      下线层数；
     * 必须在 buildTree 调用之后使用
     * @param root 起始节点
     * @param filter 有效节点过滤器，节点分为真人和虚拟（id 以 "XN_" 开头）
     */
    public void traverseEffectiveTree(TreeNode root, EffectiveNodeFilter filter) {
        if (root.getChildren() == null || root.getChildren().size() == 0) {
            if (filter.doFilter(root)) {
                root.setEffectiveBranchCount(1);
            } else {
                //虚拟节点不算
                root.setEffectiveBranchCount(0);
            }
            // 叶子节点子树节点数为1
            root.setBranchCount(1);
            // 叶子节点子树高度为0
            root.setBranchHeight(0);
        } else {
            for (TreeNode branch: root.getChildren()) {
                traverseEffectiveTree(branch, filter);
            }
            int currentBranchCount = 0;
            int currentEffectiveBranchCount = 0;
            int currentBranchHeight = 0;
            for (TreeNode branch: root.getChildren()) {
                currentBranchCount += branch.getBranchCount();
                currentEffectiveBranchCount += branch.getEffectiveBranchCount();
                if (branch.getBranchHeight() > currentBranchHeight) {//找到孩子节点高度最高的
                    currentBranchHeight = branch.getBranchHeight();
                }
            }
            if (filter.doFilter(root)) {
                root.setEffectiveBranchCount(currentEffectiveBranchCount + 1);
            } else {
                //虚拟节点不算
                root.setEffectiveBranchCount(currentEffectiveBranchCount);
            }
            // 子树节点数 +1
            root.setBranchCount(currentBranchCount + 1);
            // 子树高度 +1
            root.setBranchHeight(currentBranchHeight + 1);
        }
        traversal.put(Integer.parseInt(root.getId()),root);
    }

    /**
     * 遍历 traversal 打印 branch info
     */
    public void printBranchInfo() {
        System.out.println("打印 traversal :");
        for (Map.Entry<Integer, TreeNode> entry : traversal.entrySet()) {
            Integer id = entry.getKey();
            TreeNode node = entry.getValue();
            System.out.println(id + " 直接下线人数："+ node.getChildrenCount() +" 伞下总人数: " + (node.getBranchCount() - 1) + " 发展下线层数: " + node.getBranchHeight());
        }
    }

    /**
     * 遍历 traversal 保存图的所有信息，包括属性信息 & 计算属性
     * 计算属性包括：
     *  1.层级
     *  2.伞高
     *  3.伞下人数（包括自身）
     *  4.直接下线人数
     *  5.路径
     * @param path 文件保存路径
     */
    public void saveAllBranchInfo(String path) {
        System.out.println("saving all branch info... ");
        BufferedWriter txtWriter = null;
        try {
            String titleLine = StringUtils.join(Arrays.asList(columnTitleNames.clone()), ",");
            titleLine += ",层级,";
            titleLine += "伞高,";
            titleLine += "伞下人数（包括自身）,";
            titleLine += "直接下线人数,";
            titleLine += "路径\n";
            System.out.println("Title: \n"+titleLine);

            txtWriter = new BufferedWriter(new FileWriter(path));
            txtWriter.write(titleLine);
            for (Map.Entry<Integer, TreeNode> entry : traversal.entrySet()) {
                Integer id = entry.getKey();
                TreeNode node = entry.getValue();
                String branchLine = "";
                for (String key : columnTitleNames) {
                    if (key.equals("id")) {
                        branchLine += (node.getId() + ",");
                    } else {
                        branchLine += (node.getNodeProperties().getPropertyMap().get(key) + ",");
                    }
                }
                branchLine += (node.getLevel() + ",");
                branchLine += (node.getBranchHeight() + ",");
                branchLine += (node.getBranchCount() + ",");
                branchLine += (node.getChildrenCount() + ",");
                branchLine += (getPathStringFromRootById(id, ">") + ",\n");
                txtWriter.write(branchLine);
            }
            txtWriter.flush();
            txtWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 判断节点是否是叶子
     * @param id 节点id
     * @return
     */
    public boolean isLeaf(Integer id) {
        if (outDegree.get(id).intValue() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 新的获得叶子节点方法，尝试加快速度
     * @param id
     * @param currentLevel
     * @param isCollapsed
     * @return
     */
    public List<TreeNode> getLeafsFaster(Integer id, int currentLevel, boolean isCollapsed) {
        List<TreeNode> l = new ArrayList<TreeNode>();
        List<SimpleEdge> finOut = edgeCollection.get(id);
        for (SimpleEdge e: finOut) {
            l.add(new TreeNode(e.getTarget(), currentLevel + 1, isCollapsed, e.getNodeProperties(), String.valueOf(id)));
        }
        return l;
    }

    /**
     * 探测树图中的根，正常1个TreeGraph只包含1个root
     * 通常用作试探，找到根之后再设置根
     * @return
     */
    public void detectRoot() {
        for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value.intValue() == 0 && this.outDegree.get(key).intValue() >= 0) {
                // 根节点入度为0，出度大于等于0
                System.out.println(name + " root found: " + key + " " + " in/out: " + value + "/" + this.outDegree.get(key).intValue());
            }
        }
    }

    /**
     * 返回树图（森林）中的根的id列表
     * @return
     */
    public List<String> findRoots() {
        List<String> result = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value.intValue() == 0 && this.outDegree.get(key).intValue() >= 0) {
                // 根节点入度为0，出度大于等于0
                result.add(key.toString());
            }
        }
        return result;
    }

    /**
     * selfComputeInOutDegree 之后，selfBuildTree 之前，把森林变成树
     * 代替 detectRoot 和 setRoot（withLabel）
     * 尽量不再修改原始文件
     * 
     */
    public void makeBundle() {
        List<String> qualifiedRoots = findRoots();
        if (qualifiedRoots.size() > 1) {
            //有多个根，检测到森林
            //严格来说检测到多个根并不能说明一定是森林，也有可能是DAG，如果 检测到的根数量 + 边数量 = 点数量 才是森林；不过对于传销案件来说，一个人只有一个推荐人（parentId 只有一个）
            System.out.println(qualifiedRoots.size() + " qualified roots found, this MAY be a forest");
            TreeNode virtualRoot = new TreeNode(GraphVirtualConstants.VIRTUAL_ROOT_ID);
            NodeProperties nodeProperties = new NodeProperties();//虚拟根没有属性
            nodeProperties.saveProperty(columnTitleNames[idColumnIndex], GraphVirtualConstants.VIRTUAL_ROOT_ID);
            virtualRoot.setNodeProperties(nodeProperties);
            virtualRoot.setLabel(GraphVirtualConstants.VIRTUAL_ROOT_LABEL);
            this.inDegree.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), 0);
            this.outDegree.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), qualifiedRoots.size());
            List<SimpleEdge> collection = new ArrayList<>();
            for (String s : qualifiedRoots) {
                SimpleEdge simpleEdge;
                if (getTopNodeInfo().containsKey(s)) {
                    //原始文件中显示列出了顶层节点（根节点）
                    simpleEdge = new SimpleEdge(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), Integer.parseInt(s), getTopNodeInfo().get(s));
                } else {
                    // 原始文件中未列出顶层节点（根节点）
                    NodeProperties properties = new NodeProperties();
                    // 此时顶层节点的 properties 我们并不知道，如有必要，需要特别指定
                    properties.getPropertyMap().put(columnTitleNames[parentIdColumnIndex], GraphVirtualConstants.VIRTUAL_ROOT_ID);
                    // 如果提供了 idLabelMap，那label(多半是地址hash)可能是能提供的唯一属性信息
//                    if (idLabelMap != null && idLabelMap.size() > 0) {
//                        properties.getPropertyMap().put(columnTitleNames[topLevelNodeLabelColumnIndex], this.idLabelMap.get(Integer.parseInt(s)));
//                    }
                    simpleEdge = new SimpleEdge(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), Integer.parseInt(s), properties);
                }
                this.edgeSet.add(simpleEdge);
                collection.add(simpleEdge);
            }
            this.edgeCollection.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), collection);
//            if (this.idLabelMap != null) {
//                this.idLabelMap.put(Integer.parseInt(GraphVirtualConstants.VIRTUAL_ROOT_ID), GraphVirtualConstants.VIRTUAL_ROOT_LABEL);
//            }
            this.roots.add(virtualRoot);
        } else if (qualifiedRoots.size() == 1) {
            //只有一个根，检测到树
            System.out.println("1 qualified root found, this MAY be a tree");
            //为这个树根添加properties
            if (topNodeInfo.isEmpty()) {
                //topNodeInfo 为空，原始文件中没有显示提供根信息
                this.setRootWithLabel(qualifiedRoots.get(0), new NodeProperties());//根属性为空
            } else {
                //topNodeInfo 不为空，原始文件中显示地提供了根信息
                this.setRootWithLabel(qualifiedRoots.get(0), topNodeInfo.get(qualifiedRoots.get(0)));//根属性为原始文件中显示指定的属性信息
            }
        }
    }

    /**
     * 按照 节点id 手动设定图的根
     * 通常使用 detectRoot 方法试探出根，然后手动使用该方法设置根
     * @param id
     */
    public void setRoot(String id) {
        roots.add(new TreeNode(id, 0,true));
    }

    /**
     * 按照 节点id 手动设定图的根
     * 为了解决root的properties获取不到问题，需要再从文件中获取一次
     * @param id
     */
    public void setRoot(String id, String path, int idIndex) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            // 第一行信息，为标题信息
            String titleLine = reader.readLine();
            String columnName[] = titleLine.split(",");
            // 第一行有多少个元素，
            int columnCount = columnName.length;
            String line = null;
            while ((line = reader.readLine()) != null) {
                String item[] = line.split(",",-1);
                // 在文件里找到第一个root
                if (item.length == columnCount && item[idIndex].equals(id)) {
                    // 初始化NodeProperties并赋给SimpleEdge
                    NodeProperties properties = new NodeProperties();
                    for (int i = 0; i < columnCount; i ++) {
                        properties.saveProperty(columnName[i], item[i]);
                    }
                    TreeNode rootNode = new TreeNode(id, 0,true);
                    rootNode.setNodeProperties(properties);
                    roots.add(rootNode);
                    break;
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 按照 节点id 手动设定图的根
     * 设置根的同时，添加label
     * @param id
     */
    public void setRootWithCertainLabel(String id, String label) {
        roots.add(new TreeNode(id, label,0,true));
    }

    /**
     * 按照 节点id 手动设定图的根
     * 设置根的同时，添加label，前提是设定了id-label map
     * 为根添加指定 properties
     * @param id
     * @param properties
     */
    public void setRootWithLabel(String id, NodeProperties properties) {
        roots.add(new TreeNode(id, "",0,true, properties));
    }

    /**
     * 打印层序信息
     */
    public void printLevelInfo() {
        System.out.println(name + " levelSet: ");
        for (Map.Entry<Integer, Set<Integer>> entry : levelSet.entrySet()) {
            Integer levelId = entry.getKey();
            Set<Integer> levelElements = entry.getValue();
            System.out.println(levelId + " :" + levelElements.toString());
        }
    }

    /**
     * 保存层序信息（推荐txt）
     * @param path
     */
    public void saveLevelInfo(String path) {
        String levelInfo = "";
        System.out.println("saving levelSet: " + levelSet.size() + " levels");
        BufferedWriter jsonWriter = null;
        try {
            jsonWriter = new BufferedWriter(new FileWriter(path));
            for (Map.Entry<Integer, Set<Integer>> entry : levelSet.entrySet()) {
                Integer levelId = entry.getKey();
                Set<Integer> levelElements = entry.getValue();
                levelInfo = levelId + " :" + levelElements.toString() + "\n";
                jsonWriter.write(levelInfo);
            }
            jsonWriter.flush();
            jsonWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 按根序号打印树图内容
     * @param rootIndex
     */
    public void printTreeGraphJson(int rootIndex) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = trimUnnecessaryContent(mapper.writeValueAsString(roots.get(rootIndex)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println( name + " json:\n" + json);
    }

    /**
     * 按根序号保存树图内容（推荐json）
     * @param rootIndex 根序号，通常是第一个
     * @param path 输出文件路径
     */
    public void saveTreeGraphJson(int rootIndex, String path) {
        System.out.println("saving json... ");
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = trimUnnecessaryContent(mapper.writeValueAsString(roots.get(rootIndex)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        BufferedWriter jsonWriter = null;
        try {
            jsonWriter = new BufferedWriter(new FileWriter(path));
            jsonWriter.write(json);
            jsonWriter.flush();
            jsonWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取TreeNode的json表示
     * @param treeNode 目标节点
     * @return
     */
    public String getTreeNodeJson(TreeNode treeNode) {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = trimUnnecessaryContent(mapper.writeValueAsString(treeNode));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 获取TreeNode的json表示
     * @param nodeId 目标节点id
     * @return
     */
    public String getTreeNodeJson(int nodeId){
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = trimUnnecessaryContent(mapper.writeValueAsString(traversal.get(nodeId)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 去除json中无用的字段，主要是空children字段
     *
     * @param old
     * @return
     */
    private String trimUnnecessaryContent(String old){
        return old.replace(",\"children\":null", "")
                .replace("\uFEFF", "");
    }

    /**
     * 对图所有节点（保存在traversal中）按 BranchCount 排序
     * @param topLimit 排序后取前多少个
     */
    public void getTopBranchCount(int topLimit) {
        long startTime = System.currentTimeMillis();
        List<Map.Entry<Integer, TreeNode>> list = new ArrayList<Map.Entry<Integer, TreeNode>>(traversal.entrySet());
        // 降序排序
        Collections.sort(list, (o1, o2) -> o2.getValue().getBranchCount() - o1.getValue().getBranchCount());
        int count = 0;
        for (Map.Entry<Integer, TreeNode> mapping: list) {
            if (count < topLimit) {
                topBranchCount.add(mapping.getKey());
            } else {
                break;
            }
            count ++;
        }
        long endTime = System.currentTimeMillis();
        System.out.println("get Top " + topLimit + " BranchCount in " + ( endTime - startTime ) + " ms : " + topBranchCount.toString());
    }

    /**
     * 对图所有节点（保存在traversal中）按 BranchHeight 排序
     * @param topLimit 排序后取前多少个
     */
    public void getTopBranchHeight(int topLimit) {
        long startTime = System.currentTimeMillis();
        List<Map.Entry<Integer, TreeNode>> list = new ArrayList<Map.Entry<Integer, TreeNode>>(traversal.entrySet());
        //降序排序
        Collections.sort(list, (o1, o2) -> o2.getValue().getBranchHeight() - o1.getValue().getBranchHeight());
        int count = 0;
        for (Map.Entry<Integer, TreeNode> mapping: list) {
            if (count < topLimit) {
                topBranchHeight.add(mapping.getKey());
            } else {
                break;
            }
            count ++;
        }
        long endTime = System.currentTimeMillis();
        System.out.println("get Top " + topLimit + " BranchHeight in " + ( endTime - startTime ) + " ms : " + topBranchHeight.toString());
    }

    /**
     * 对图所有节点（保存在traversal中）按 ChildrenCount 排序
     * @param topLimit 排序后取前多少个
     */
    public void getTopChildrenCount(int topLimit) {
        long startTime = System.currentTimeMillis();
        List<Map.Entry<Integer, TreeNode>> list = new ArrayList<Map.Entry<Integer, TreeNode>>(traversal.entrySet());
        //降序排序
        Collections.sort(list, (o1, o2) -> {
            int count1 = 0;
            int count2 = 0;
            if (o1.getValue().getChildren() != null) {
                count1 = o1.getValue().getChildren().size();
            }
            if (o2.getValue().getChildren() != null) {
                count2 = o2.getValue().getChildren().size();
            }
            return count2 - count1;
        });
        int count = 0;
        for (Map.Entry<Integer, TreeNode> mapping: list) {
            if (count < topLimit) {
                topChildrenCount.add(mapping.getKey());
            } else {
                break;
            }
            count ++;
        }
        long endTime = System.currentTimeMillis();
        System.out.println("get Top " + topLimit + " ChildrenCount in " + ( endTime - startTime ) + " ms : " + topChildrenCount.toString());
    }

    /**
     * 获取伞下所有人列表（包括自己）
     * @param root 目标节点
     * @param isCollapsed 是否展开
     * @param levelLimit 伞下拓展层数
     * @param descendantsList 结果列表
     */
    public void getAllDescendants(TreeNode root, boolean isCollapsed, int levelLimit, List<String> descendantsList) {
        String currentId = root.getId();
        descendantsList.add(currentId);
        if (isLeaf(Integer.parseInt(root.getId()))) {
            root.setCollapsed(false);
        } else {
            if (levelLimit == 0) {
                for (TreeNode branch: root.getChildren()) {
                    getAllDescendants(branch, isCollapsed, levelLimit, descendantsList);
                }
            } else if (root.getSubLevel() < levelLimit) {// copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    getAllDescendants(branch, isCollapsed, levelLimit, descendantsList);
                }
            } else if (root.getSubLevel() >= levelLimit) { //切断 children 层级引用 copy子图的时候应该用 subLevel
                for (TreeNode branch: root.getChildren()) {
                    branch.setChildren(null);
                }
            }
        }
    }

    public Map<Integer, TreeNode> getTraversal() {
        return traversal;
    }

    public void setTraversal(Map<Integer, TreeNode> traversal) {
        this.traversal = traversal;
    }

    public Map<Integer, Set<Integer>> getLevelSet() {
        return levelSet;
    }

    public void setLevelSet(Map<Integer, Set<Integer>> levelSet) {
        this.levelSet = levelSet;
    }

    public List<TreeNode> getRoots() {
        return roots;
    }

    public void setRoots(List<TreeNode> roots) {
        this.roots = roots;
    }

    public Map<String, NodeProperties> getTopNodeInfo() {
        return this.topNodeInfo;
    }

    public int getIdColumnIndex() {
        return idColumnIndex;
    }

    public void setIdColumnIndex(int idColumnIndex) {
        this.idColumnIndex = idColumnIndex;
    }

    public int getParentIdColumnIndex() {
        return parentIdColumnIndex;
    }

    public void setParentIdColumnIndex(int parentIdColumnIndex) {
        this.parentIdColumnIndex = parentIdColumnIndex;
    }


}
