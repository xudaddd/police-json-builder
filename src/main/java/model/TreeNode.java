package model;

import custom.EffectiveNodeFilter;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 节点
 * 主要用途：构建&存储传销层级树中人
 *
 *
 *
 * @author xuda
 */
@Data
@NoArgsConstructor
public class TreeNode extends SimpleNode {
    /**
     * 节点收起/展开
     */
    private boolean collapsed;

    /**
     * 节点所在层级，从树根开始算，树根为0层
     */
    private int level;

    /**
     * 子树高度
     * 主要应用场景：在获取子树的时候，用subLevel记录该节点在子树中的层级
     * 如果涉及生成子图，考虑把它放在继承TreeNode的子类中，直接放在这可能是不好的设计
     */
    private int subLevel = -1;

    /**
     * 所有子孙数量 (包含子树根)
     */
    private int branchCount;

    /**
     * 子树高度 (下级层数)，叶子节点子树高度为0
     */
    private int branchHeight;

    /**
     * 节点的直接后代
     */
    protected List<TreeNode> children;

    /**
     * 父节点id
     */
    protected String parentId;

    /**
     * 8月3日，为了响应杨亮3M案件，添加label属性
     */
    protected String label;

    /**
     * 附属在节点上的属性列表
     */
    private NodeProperties nodeProperties;

    /**
     * 有效下线数
     *
     */
    private int effectiveBranchCount;

    public TreeNode(Integer id) {
        this.id = String.valueOf(id);
    }

    public TreeNode(String id) {
        this.id = id;
    }

    public TreeNode(Integer id, boolean isCollapsed) {
        this.collapsed = isCollapsed;
        this.id = String.valueOf(id);
    }

    public TreeNode(Integer id, int level, boolean isCollapsed) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = String.valueOf(id);
    }

    public TreeNode(Integer id, int level, boolean isCollapsed, NodeProperties nodeProperties, String parentId ) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = String.valueOf(id);
        this.nodeProperties = nodeProperties;
        this.parentId = parentId;
    }

    public TreeNode(Integer id, int level, boolean isCollapsed, NodeProperties nodeProperties, String parentId, String label ) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = String.valueOf(id);
        this.nodeProperties = nodeProperties;
        this.parentId = parentId;
        this.label = label;
    }

    public TreeNode(String id, int level, boolean isCollapsed) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = id;
    }

    public TreeNode(String id, String label, int level, boolean isCollapsed) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = id;
        this.label = label;
    }

    public TreeNode(String id, String label, int level, boolean isCollapsed, NodeProperties properties) {
        this.level = level;
        this.collapsed = isCollapsed;
        this.id = id;
        this.label = label;
        this.nodeProperties = properties;

    }

    /**
     * 获得直接下级数
     * @return
     */
    public int getChildrenCount() {
        if (children != null) {
            return children.size();
        } else {
            return 0;
        }
    }

    /**
     * 获得 有效 直接下级数
     * @return
     */
    public int getEffectiveChildrenCount(EffectiveNodeFilter filter) {
        if (children != null) {
            int result = 0;
            for (TreeNode child : children) {
                if (filter.doFilter(child)) {
                    result++;
                }
            }
            return result;
        } else {
            return 0;
        }
    }

    public String[] getPropertyKeys() {
        if (nodeProperties != null) {
            return nodeProperties.getPropertyKeys();
        } else {
            return null;
        }
    }


}
