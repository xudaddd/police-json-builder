package model.temp;

import lombok.Data;

/***
 * 6月7日案件需求 特定容器 为了读取 user-info-march.csv
 * @author xuda
 * @deprecated
 */
@Data
public class ReportItem extends UserInfoMarch {
    protected String real_name = "";
    protected String id_number = "";

    /**
     * 正常是不会出现-1的
     */
    protected Integer sub_level = -1;
    protected String path = "";

    public void setProperties(UserInfoMarch info) {
        super.id = info.getId();
        super.parent_id = info.getParent_id();
        super.rank = info.getRank();
        super.country_code = info.getCountry_code();
        super.phone = info.getPhone();
        super.password = info.getPassword();
        super.fund_pass = info.getFund_pass();
        super.nickname = info.getNickname();
        super.avatar = info.getAvatar();
        super.invite_code = info.getInvite_code();
        super.token = info.getToken();
        super.hash = info.getHash();
        super.level = info.getLevel();
        super.grade = info.getGrade();
        super.status = info.getStatus();
        super.fund_status = info.getFund_status();
        super.im_status = info.getIm_status();
        super.active = info.getActive();
        super.note = info.getNote();
        super.reg_time = info.getReg_time();
        super.reg_ip = info.getReg_ip();
        super.reg_address = info.getReg_address();
        super.did = info.getDid();
        super.read_time = info.getRead_time();
        super.start_at = info.getStart_at();
        super.expire_at = info.getExpire_at();
        super.proxy = info.getProxy();
        super.is_work = info.getIs_work();
        super.work_at = info.getWork_at();
        super.created_at = info.getCreated_at();
        super.updated_at = info.getUpdated_at();
    }

    @Override
    public String toString() {
        String string =
                "----id: " + this.getId() + "\n" +
                        "real_name: " + this.getReal_name() + "\n" +
                        "id_number: " + this.getId_number() + "\n" +
                        "sub_level: " + this.getSub_level() + "\n" +
                        "path: " + this.getPath() + "\n" +
                "parent_id: " + this.getParent_id() + "\n" +
                "rank: " + this.getRank() + "\n" +
                "country_code: " + this.getCountry_code() + "\n" +
                "phone: " + this.getPhone() + "\n" +
                "password: " + this.getPassword() + "\n" +
                "fund_pass: " + this.getFund_pass() + "\n" +
                "nickname: " + this.getNickname() + "\n" +
                "avatar: " + this.getAvatar() + "\n" +
                "invite_code: " + this.getInvite_code() + "\n" +
                "token: " + this.getToken() + "\n" +
                "hash: " + this.getHash() + "\n" +
                "level: " + this.getLevel() + "\n" +
                "grade: " + this.getGrade() + "\n" +
                "status: " + this.getStatus() + "\n" +
                "fund_status: " + this.getFund_status() + "\n" +
                "im_status: " + this.getIm_status() + "\n" +
                "active: " + this.getActive() + "\n" +
                "note: " + this.getNote() + "\n" +
                "reg_time: " + this.getReg_time() + "\n" +
                "reg_ip: " + this.getReg_ip() + "\n" +
                "reg_address: " + this.getReg_address() + "\n" +
                "did: " + this.getDid() + "\n" +
                "read_time: " + this.getRead_time() + "\n" +
                "start_at: " + this.getStart_at() + "\n" +
                "expire_at: " + this.getExpire_at() + "\n" +
                "proxy: " + this.getProxy() + "\n" +
                "is_work: " + this.getIs_work() + "\n" +
                "work_at: " + this.getWork_at() + "\n" +
                "created_at: " + this.getCreated_at() + "\n" +
                "updated_at: " + this.getUpdated_at() + "\n-------";
        return string;
    }
}
