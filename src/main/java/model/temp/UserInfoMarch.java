package model.temp;

import lombok.Data;

/***
 * 6月7日案件需求 特定容器 为了读取 user-info-march.csv
 * @author xuda
 * @deprecated
 */
@Data
public class UserInfoMarch {
    protected String id = "";
    protected String parent_id = "";
    protected String rank = "";
    protected String country_code = "";
    protected String phone = "";
    protected String password = "";
    protected String fund_pass = "";
    protected String nickname = "";
    protected String avatar = "";
    protected String invite_code = "";
    protected String token = "";
    protected String hash = "";
    protected Integer level = 0;
    protected String grade = "";
    protected String status = "";
    protected String fund_status = "";
    protected String im_status = "";
    protected String active = "";
    protected String note = "";
    protected String reg_time = "";
    protected String reg_ip = "";
    protected String reg_address = "";
    protected String did = "";
    protected String read_time = "";
    protected String start_at = "";
    protected String expire_at = "";
    protected String proxy = "";
    protected String is_work = "";
    protected String work_at = "";
    protected String created_at = "";
    protected String updated_at = "";
}
