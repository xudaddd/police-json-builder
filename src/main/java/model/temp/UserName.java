package model.temp;

import lombok.Data;

/***
 * 6月7日案件需求 特定容器 为了读取 user-name.csv
 * @author xuda
 * @deprecated
 */
@Data
public class UserName {
    private String id  = "";
    private String user_id = "";
    private String real_name = "";
    private String id_number = "";
    private String image = "";
    private String status = "";
    private String note = "";
    private String created_at = "";
    private String updated_at = "";
}
