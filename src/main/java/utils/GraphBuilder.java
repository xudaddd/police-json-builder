package utils;

import model.NodeProperties;
import model.SimpleEdge;
import model.TreeGraph;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.*;
import java.util.*;

/**
 *
 * Graph构造器
 * 标准文件格式模板: resources/template/template-0.csv
 * 至少有两列数字，推荐第一列为id，第二列为parentId
 *
 *
 * @author xuda
 *
 */
public class GraphBuilder{

    /**
     * 本地文件格式检测
     * @param path
     * @return
     */
    public boolean testFileFormat(String path) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String titleLine = reader.readLine();
            String[] titles = titleLine.split(",");
            int titleCount = titles.length;
            System.out.println("title column count： " + titleCount);
            String firstLine = reader.readLine();
            String[] cells = firstLine.split(",");
            int cellCount = cells.length;
            System.out.println("first line cells count: " + cellCount);

            //如果第一行和标题行按逗号划分的分段数量相同
            return titleCount == cellCount ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将行内容分隔成列
     * @param line 输入行内容
     * @param isDoubleQuote 列内容是否被双引号包裹
     * @return
     */
    private String[] splitLine(String line, boolean isDoubleQuote) {
        if (!isDoubleQuote) {
            //单引号分隔
            String[] result = line.split(",", -1);
            return result;
        } else {
            //双引号分隔
            String[] segs = line.split(",",-1);
            String[] result = new String[segs.length];
            for (int i = 0; i < segs.length; i ++) {
                result[i] = segs[i].replaceAll("\"", "");
            }
            return result;
        }
    }

    /**
     * 判断标题列是否被双引号包裹
     * 一般来说，原始文件至少含两列数据，只要观察前两列列名即可
     * 如果前两列都带双引号，大概率该文件所有内容被双引号包裹
     * @param column
     * @return
     */
    private boolean isTitleDoubleQuoted(String[] column) {
        return column[0].startsWith("\"") && column[0].endsWith("\"") && column[1].startsWith("\"") && column[1].endsWith("\"");
    }

    /**
     * 通用文件读取方法，自定义 id 列 和 parentId 列 序号，其他列随意
     * 建议id列列名为"id"
     * 列内容带或不带双引号均可
     * @param t 树图实例
     * @param graphName 图名字
     * @param path 原始文件存储路径
     * @param idIndex 原始文件中节点列序号
     * @param parentIndex 原始文件中父节点列序号
     * @return 目标树图
     */
    public <T extends TreeGraph> T buildTreeGraphFromCSV(T t, String graphName, String path, int idIndex, int parentIndex) {
        try{
//            T treeGraph = t;
            BufferedReader reader = new BufferedReader(new FileReader(path));
            //默认双引号标记为false
            boolean doubleQuote = false;

            // 第一行信息，为标题信息
            String titleLine = reader.readLine();
            String[] columnName = titleLine.split(",");
            if (columnName.length > 0 && isTitleDoubleQuoted(columnName)) {
                doubleQuote = true;
                columnName = splitLine(titleLine, doubleQuote);
            }

            // 第一行有多少个元素
            int columnCount = columnName.length;
            System.out.println("文件列数： " + columnCount);

            String line = null;
            Set<Integer> idSet = new HashSet<Integer>();
            while((line = reader.readLine()) != null) {
                String[] item = splitLine(line, doubleQuote);
                if (item.length == columnCount) {
                    if (!item[parentIndex].isEmpty()) {
                        Integer id1 = Integer.parseInt(item[idIndex]);
                        // 注意有的时候parentId列为空
                        Integer id2 = Integer.parseInt(item[parentIndex]);
                        idSet.add(id1);
                        idSet.add(id2);
                        t.getInDegree().put(id1, 0);
                        t.getOutDegree().put(id1, 0);
                        t.getInDegree().put(id2, 0);
                        t.getOutDegree().put(id2, 0);

                        int source = id2;
                        int target = id1;
                        // parent_id 指向 id
                        SimpleEdge simpleEdge = new SimpleEdge(source, target);
                        // 初始化NodeProperties并赋给SimpleEdge
                        NodeProperties properties = new NodeProperties();
                        for (int i = 0; i < columnCount; i++) {
                            properties.saveProperty(columnName[i], item[i]);
                        }
                        simpleEdge.setNodeProperties(properties);
                        t.getEdgeSet().add(simpleEdge);
                    } else {
                        // 文件中显示列出顶层节点（根节点）
                        // 顶层节点（根节点）没有parentId
                        Integer id1 = Integer.parseInt(item[idIndex]);
                        idSet.add(id1);
                        t.getInDegree().put(id1, 0);
                        t.getOutDegree().put(id1, 0);

                        NodeProperties properties = new NodeProperties();
                        for (int i = 0; i < columnCount; i++) {
                            properties.saveProperty(columnName[i], item[i]);
                        }
                        t.getTopNodeInfo().put(item[idIndex], properties);
                    }
                } else {
                    System.out.println("invalid line: " + line);
                    System.out.println(" 本行列数：" + item.length);
                }
            }
            t.selfComputeInOutDegree();
            t.setColumnTitleNames(columnName);
            t.setName(graphName);
            t.setIdColumnIndex(idIndex);
            t.setParentIdColumnIndex(parentIndex);
            System.out.println("nodes count in file: " + idSet.size());
            System.out.println("edges count in file: " + t.getEdgeSet().size());
            return t;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 向不带id的文件添加 id parentId列
     * 有时候客户给过来的文件只有hash地址，或者其他标识符
     *  如果原始文件带有双引号，本方法会滤掉所有双引号
     * @param oldPath 原始文件路径
     * @param newPath 生成带id parentId文件路径
     * @param labelIndex 原始文件中节点标识列序号
     * @param parentLabelIndex 原始文件中父节点标识序号
     */
    public Map<Integer, String> generateFileWithId(String oldPath, String newPath, int labelIndex, int parentLabelIndex) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(oldPath));
            Map<String, Integer> hashIdMap = new HashMap<>();
            // 默认双引号标记为false
            boolean doubleQuote = false;
            String titleLine = reader.readLine();
            String[] columnName = titleLine.split(",", -1);
            if (columnName.length > 0 && isTitleDoubleQuoted(columnName)) {
                doubleQuote = true;
                columnName = splitLine(titleLine, doubleQuote);
            }
            // 列数量
            int columnCount = columnName.length;
            // 标识去重
            Set<String> hashSet = new HashSet<>();
            // 去重边
//            Set<TreeEdge> treeEdgeSet = new HashSet<>();
            // 保存原始内容
            List<String[]> originalContent = new ArrayList<>();
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] segs = splitLine(line, doubleQuote);
                originalContent.add(segs);
                if (segs.length == columnCount) {
                    String startAddr = segs[parentLabelIndex];
                    String endAddr = segs[labelIndex];
                    hashSet.add(startAddr);
                    hashSet.add(endAddr);
//                    TreeEdge edge = new TreeEdge(startAddr, endAddr);
//                    treeEdgeSet.add(edge);
                } else {
                    System.out.println("invalid line: " + segs[0]);
                    System.out.println(" 本行列数：" + segs.length);
                }
            }
            // 从1开始为addr编号
            int index = 1;
            for (String s : hashSet) {
                hashIdMap.put(s, index);
                index++;
            }
            System.out.println("标识 size: " + hashSet.size());
//            System.out.println("边 size: " + treeEdgeSet.size());
            System.out.println("标识-id对应表 size: " + hashIdMap.size());

            //生成我们需要的格式
            BufferedWriter txtWriter = new BufferedWriter(new FileWriter(newPath));
            txtWriter.write(StringUtils.join(reformColumn(columnName),",") + "\n");
            for (String[] segs : originalContent) {
                txtWriter.write(StringUtils.join(reformContentLine(hashIdMap, labelIndex, parentLabelIndex, segs), ",") + "\n");
            }
            reader.close();
            txtWriter.flush();
            txtWriter.close();
            return reverseMap(hashIdMap);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 重新写标题行
     * @param column
     * @return
     */
    private String[] reformColumn(String[] column) {
        int size = column.length + 2;
        String[] result = new String[size];
        result[0] = "id";
        result[1] = "parentId";
        for (int i = 0; i < column.length; i ++) {
            result[i + 2] = column[i];
        }
        return result;
    }

    /**
     * 重写内容行，在最前面加两列：第0列为id，第1列为parentId
     * @param map
     * @param labelIndex
     * @param parentLabelIndex
     * @param oldContent
     * @return
     */
    private String[] reformContentLine(Map<String, Integer> map, int labelIndex, int parentLabelIndex, String[] oldContent) {
        int size = oldContent.length + 2;
        String[] result = new String[size];
        result[0] = map.get(oldContent[labelIndex]).toString();
        result[1] = map.get(oldContent[parentLabelIndex]).toString();
        for (int i = 0; i < oldContent.length; i ++) {
            result[i + 2] = oldContent[i];
        }
        return result;
    }

    private Map<Integer, String> reverseMap(Map<String, Integer> map) {
        Map<Integer, String> result = new HashMap<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            result.put(entry.getValue(), entry.getKey());
        }
        return result;
    }

    /***
     * 导出excel，列顺序与原始文件保持一致
     * @param path 输出excel路径
     * @param sheetName sheet名称
     * @param list 计算结果属性列表
     * @param templatePath 原始文件路径
     */
    public void exportToExcel(String path, String sheetName, List<NodeProperties> list, String templatePath){
        try{
            // 读取模板txt/csv的title行，确定列顺序
            BufferedReader reader = new BufferedReader(new FileReader(templatePath));
            // 第一行信息，为标题信息
            String titleLine = reader.readLine();
            String[] templateColumn = titleLine.split(",");
            SXSSFWorkbook workbook= new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet(sheetName);
            SXSSFRow titleRow = sheet.createRow(0);
            if(list == null || list.size() == 0) {
                System.out.println("excel结果为空");
            }
            int columnIndex = 0;
            for(String s : templateColumn){
                SXSSFCell titleCell = titleRow.createCell(columnIndex);
                titleCell.setCellValue(s);
                columnIndex ++;
            }
            //写内容行
            int startRow = 1;
            for(NodeProperties p : list) {
                SXSSFRow row = sheet.createRow(startRow);
                int startColumn = 0;
                for(String s : templateColumn){
                    SXSSFCell cell = row.createCell(startColumn);
                    cell.setCellValue(p.getPropertyMap().get(s));
                    startColumn ++;
                }
                startRow ++;
            }
            FileOutputStream os = new FileOutputStream(path);
            workbook.write(os);
            os.flush();
            os.close();
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}