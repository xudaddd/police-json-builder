package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import constant.GraphPropertyConstants;
import model.TreeNode;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * 图谱内容恢复
 * @author xuda
 * @date 2021-11-08
 */
public class GraphRecovery {

    /**
     * 把前端内容恢复成文件
     * 样例数据，除了 id 和 parentId 列，还有 name, identity, email
     *
     * @param content
     * @param path
     * @throws IOException
     */
    public static void restoreGraphToOriginalExampleCSV(String content, String path) throws IOException {
        TreeNode result = GraphRecovery.restoreToOriginal(content);
        List<TreeNode> out = new ArrayList<>();
//        System.out.println(result.getId());
        GraphRecovery.buildPlainText(result, out);
//        System.out.println("original size: " + out.size());
        String[] propertyKeys = result.getPropertyKeys();
        String titleLine = GraphPropertyConstants.PROPERTY_ID + "," + GraphPropertyConstants.PROPERTY_PARENT_ID + ",name,identity,email";
//        System.out.println(propertyKeys);
        if (propertyKeys != null && propertyKeys.length > 0) {
            for (String key : propertyKeys) {
                titleLine += ("," + key);
            }
        }
        titleLine += "\n";

        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(titleLine);
        for (TreeNode node : out) {
            String line = node.getId() + "," + node.getParentId() + "," + PropertyGenerator.getPersonName(2) + "," + PropertyGenerator.getRandomIdentity() + "," + PropertyGenerator.getRandomEmail();
            if (node.getNodeProperties() != null && node.getNodeProperties().getPropertyMap() != null) {
                for (String s : propertyKeys) {
                    line += ("," + node.getNodeProperties().getPropertyMap().get(s));
                }
            }
            line += "\n";
            writer.write(line);
        }
        writer.flush();
        writer.close();
    }

    /**
     * 把前端内容恢复成文件
     * 只包含 id parentId 列
     * @param content
     * @param path
     * @throws IOException
     */
    public static void restoreGraphToOriginalCSV(String content, String path) throws IOException {
        TreeNode result = GraphRecovery.restoreToOriginal(content);
        List<TreeNode> out = new ArrayList<>();
//        System.out.println(result.getId());
        GraphRecovery.buildPlainText(result, out);
//        System.out.println("original size: " + out.size());
        String[] propertyKeys = result.getPropertyKeys();
        String titleLine = GraphPropertyConstants.PROPERTY_ID + "," + GraphPropertyConstants.PROPERTY_PARENT_ID;
//        System.out.println(propertyKeys);
        if (propertyKeys != null && propertyKeys.length > 0) {
            for (String key : propertyKeys) {
                titleLine += ("," + key);
            }
        }
        titleLine += "\n";

        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(titleLine);
        for (TreeNode node : out) {
            String line = node.getId() + "," + node.getParentId();
            if (node.getNodeProperties() != null && node.getNodeProperties().getPropertyMap() != null) {
                for (String s : propertyKeys) {
                    line += ("," + node.getNodeProperties().getPropertyMap().get(s));
                }
            }
            line += "\n";
            writer.write(line);
        }
        writer.flush();
        writer.close();
    }

    /**
     * 递归遍历树
     */
    public static void buildPlainText(TreeNode root, List<TreeNode> list) {
        list.add(root);
        if (root.getChildren() != null) {
            for (TreeNode branch : root.getChildren()) {
                buildPlainText(branch, list);
            }
        }
    }


    /**
     * 把 AntV6 使用的前端数据格式恢复成 TreeNode
     * @param str json 字符串
     * @return
     */
    public static TreeNode restoreToOriginal(String str) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return mapper.readValue(str, TreeNode.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
