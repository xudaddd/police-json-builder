package utils;


import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class PropertyGenerator {
    private static final int WITHDRAW_ADDR_MAX_NUM = 10;
    private static final int DEPOSIT_ADDR_MAX_NUM = 10;
    private static final int IP_MAX_NUM = 50;

    private static final int ADDRESS_LENGTH = 34;//地址位数
    private static final int WECHAT_LENGTH = 15;//微信id位数
    private static final int ALIPAY_LENGTH = 15;//支付宝id位数
    private static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LETTERCHAR = "abcdefghijkllmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMBERCHAR = "0123456789";
    private static int step = 0;


    /**
     * 整数生成器
     * @param max
     * @return
     */
    private static final int getRandomInt(int max){
        return new Double(Math.floor(Math.random() * max)).intValue();
    }

    /**
     * 从列表中随机挑选项目
     * @param list 备选项目列表
     * @return
     */
    private static String randomPick (String[] list){
        double locator = Math.floor(Math.random() * list.length);
        return list[(new Double(locator)).intValue()];
    }


    /**
     * 获得随机时间戳
     * @return
     */
    public static String getRandomTime(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(randomDate("2000-01-01","2021-01-31"));
    }


    /**
     * 在 开始时间 和 结束时间 内返回随机时间
     * @param beginDate 开始时间
     * @param endDate 结束时间
     * @return
     */
    private static Date randomDate(String beginDate, String endDate){
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date start = format.parse(beginDate);
            Date end = format.parse(endDate);

            if(start.getTime() >= end.getTime()){
                return null;
            }
            long date = random(start.getTime(),end.getTime());
            return new Date(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static long random(long begin,long end){
        long rtn = begin + (long)(Math.random() * (end - begin));
        if(rtn == begin || rtn == end){
            return random(begin,end);
        }
        return rtn;
    }

    private static String getRandomString(int len) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < len; i++) {
            sb.append(ALLCHAR.charAt(random.nextInt(ALLCHAR.length())));
        }
        return sb.toString();
    }

    public static String getRandomAddr() {
        return getRandomString(ADDRESS_LENGTH);
    }

    public static String getRandomIp() {
        // ip范围
        int[][] range = {
                {607649792, 608174079}, // 36.56.0.0-36.63.255.255
                {1038614528, 1039007743}, // 61.232.0.0-61.237.255.255
                {1783627776, 1784676351}, // 106.80.0.0-106.95.255.255
                {2035023872, 2035154943}, // 121.76.0.0-121.77.255.255
                {2078801920, 2079064063}, // 123.232.0.0-123.235.255.255
                {-1950089216, -1948778497}, // 139.196.0.0-139.215.255.255
                {-1425539072, -1425014785}, // 171.8.0.0-171.15.255.255
                {-1236271104, -1235419137}, // 182.80.0.0-182.92.255.255
                {-770113536, -768606209}, // 210.25.0.0-210.47.255.255
                {-569376768, -564133889}, // 222.16.0.0-222.95.255.255
        };
        Random random = new Random();
        int index = random.nextInt(10);
        String ip = num2ip(range[index][0] + new Random().nextInt(range[index][1] - range[index][0]));
        return ip;
    }

    /**
     * 将十进制转换成IP地址
     */
    private static String num2ip(int ip) {
        int[] b = new int[4];
        String ipStr = "";
        b[0] = (int) ((ip >> 24) & 0xff);
        b[1] = (int) ((ip >> 16) & 0xff);
        b[2] = (int) ((ip >> 8) & 0xff);
        b[3] = (int) (ip & 0xff);
        ipStr = Integer.toString(b[0]) + "." + Integer.toString(b[1]) + "." + Integer.toString(b[2]) + "." + Integer.toString(b[3]);
        return ipStr;
    }


    /**
     * 需要传入一个前缀：6、8、9中的一个。
     * 其中：6：类型1，8：类型2，9：类型3 【根据自己的业务定义】
     * 其他则会返回异常
     * @param prefix
     * @return
     */
    public synchronized static String getBankNumber(String prefix) {
        if (prefix != null && prefix != "") {
            if ("689".indexOf(prefix) >= 0 && prefix.length() == 1) {
                String st = "666" + prefix + getUnixTime();
                return st + getBankCardCheckCode(st);
            } else {
                System.out.println("银行卡号前缀有误");
                return "";
            }
        } else {
            System.out.println("银行卡号去前缀不能是空");
            return "";
        }
    }

    /**
     * 获取当前系统时间戳 并截取
     * @return
     */
    private synchronized static String getUnixTime() {
        try {
            Thread.sleep(10);//线程同步执行，休眠10毫秒 防止卡号重复
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        step++;
        step = step > 100 ? step % 10 : step;
        return ((System.currentTimeMillis() / 100) + "").substring(1) + (step % 10);
    }


    /**
     * 从不含校验位的银行卡卡号采用 Luhm 校验算法获得校验位
     * @param nonCheckCodeCardId
     * @return
     */
    private static char getBankCardCheckCode(String nonCheckCodeCardId) {
        if (nonCheckCodeCardId == null || nonCheckCodeCardId.trim().length() == 0
                || !nonCheckCodeCardId.matches("\\d+")) {
            //如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeCardId.trim().toCharArray();
        int luhmSum = 0;
        for (int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if (j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhmSum += k;
        }
        return (luhmSum % 10 == 0) ? '0' : (char) ((10 - luhmSum % 10) + '0');
    }

    /**
     * 随机生成身份证号
     * @return
     */
    public static String getRandomIdentity() {
        String identity = "";
        for (int i = 0; i < 18; i++) {
            //每次循环都从0~9挑选一个随机数
            identity += (int) (Math.random() * 10);
        }
        return identity;
    }

    /**
     * 生成随机电子邮件地址
     * @return
     */
    public static String getRandomEmail() {
        Random random = new Random();
        String number = "";
        String houzui = "";
        for (int i = 0; i < 10; i++) {
            number += random.nextInt(10);
        }
        int i = random.nextInt(3);
        if (i == 0) {
            houzui = "@qq.com";
        } else if (i == 1) {
            houzui = "@gmail.com";
        } else {
            houzui = "@163.com";
        }
        return number + houzui;
    }

    public static String getRandomWechat() {
        return getRandomString(WECHAT_LENGTH);
    }

    public static String getRandomAlipay() {
        return getRandomString(ALIPAY_LENGTH);
    }

    public static String getPhoneNum() {
        //给予真实的初始号段，号段是在百度上面查找的真实号段
        String[] start = {"133", "149", "153", "173", "177",
                "180", "181", "189", "199", "130", "131", "132",
                "145", "155", "156", "166", "171", "175", "176", "185", "186", "166", "134", "135",
                "136", "137", "138", "139", "147", "150", "151", "152", "157", "158", "159", "172",
                "178", "182", "183", "184", "187", "188", "198", "170", "171"};
        //随机出真实号段   使用数组的length属性，获得数组长度，
        //通过Math.random（）*数组长度获得数组下标，从而随机出前三位的号段
        String phoneFirstNum = start[(int) (Math.random() * start.length)];
        //随机出剩下的8位数
        String phoneLastNum = "";
        //定义尾号，尾号是8位
        final int LENPHONE = 8;
        //循环剩下的位数
        for (int i = 0; i < LENPHONE; i++) {
            //每次循环都从0~9挑选一个随机数
            phoneLastNum += (int) (Math.random() * 10);
        }
        //最终将号段和尾数连接起来
        String phoneNum = phoneFirstNum + phoneLastNum;
        return phoneNum;
    }

    /**
     * 随机生成人名
     * @param n 名字长度（不包括姓）
     * @return
     */
    public static String getPersonName(int n) {
        String ming = "";
        Random random1 = new Random();
        String[] xing = {"赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨", "朱", "秦", "尤", "许",
                "何", "吕", "施", "张", "孔", "曹", "严", "华", "金", "魏", "陶", "姜", "戚", "谢", "邹", "喻", "柏", "水", "窦", "章", "云", "苏", "潘", "葛", "奚", "范", "彭", "郎",
                "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳", "鲍", "史", "唐", "费", "廉", "岑", "薛", "雷", "贺", "倪", "汤", "滕", "殷",
                "罗", "毕", "郝", "邬", "安", "常", "乐", "于", "时", "傅", "皮", "卞", "齐", "康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄", "和",
                "穆", "萧", "尹", "姚", "邵", "湛", "汪", "祁", "毛", "禹", "狄", "米", "贝", "明", "臧", "计", "伏", "成", "戴", "谈", "宋", "茅", "庞", "熊", "纪", "舒",
                "屈", "项", "祝", "董", "梁", "杜", "阮", "蓝", "闵", "席", "季", "麻", "强", "贾", "路", "娄", "危", "江", "童", "颜", "郭", "梅", "盛", "林", "刁", "钟",
                "徐", "邱", "骆", "高", "夏", "蔡", "田", "樊", "胡", "凌", "霍", "虞", "万", "支", "柯", "昝", "管", "卢", "莫", "经", "房", "裘", "缪", "干", "解", "应",
                "宗", "丁", "宣", "贲", "邓", "郁", "单", "杭", "洪", "包", "诸", "左", "石", "崔", "吉", "钮", "龚", "程", "嵇", "邢", "滑", "裴", "陆", "荣", "翁", "荀",
                "羊", "于", "惠", "甄", "曲", "家", "封", "芮", "羿", "储", "靳", "汲", "邴", "糜", "松", "井", "段", "富", "巫", "乌", "焦", "巴", "弓", "牧", "隗", "山",
                "谷", "车", "侯", "宓", "蓬", "全", "郗", "班", "仰", "秋", "仲", "伊", "宫", "宁", "仇", "栾", "暴", "甘", "钭", "厉", "戎", "祖", "武", "符", "刘", "景",
                "詹", "束", "龙", "叶", "幸", "司", "韶", "郜", "黎", "蓟", "溥", "印", "宿", "白", "怀", "蒲", "邰", "从", "鄂", "索", "咸", "籍", "赖", "卓", "蔺", "屠",
                "蒙", "池", "乔", "阴", "郁", "胥", "能", "苍", "双", "闻", "莘", "党", "翟", "谭", "贡", "劳", "逄", "姬", "申", "扶", "堵", "冉", "宰", "郦", "雍", "却",
                "桑", "桂", "濮", "牛", "寿", "通", "边", "扈", "燕", "冀", "浦", "尚", "农", "温", "别", "庄", "晏", "柴", "瞿", "阎", "充", "慕", "连", "茹", "习",
                "宦", "艾", "鱼", "容", "向", "古", "易", "慎", "戈", "廖", "庾", "终", "暨", "居", "衡", "步", "都", "耿", "满", "弘", "匡", "国", "文", "寇", "广", "禄",
                "阙", "东", "欧", "殳", "沃", "利", "蔚", "越", "夔", "隆", "师", "巩", "厍", "聂", "晁", "勾", "敖", "融", "冷", "訾", "辛", "阚", "那", "简", "饶", "空",
                "曾", "毋", "沙", "乜", "养", "鞠", "须", "丰", "巢", "关", "蒯", "相", "查", "后", "荆", "红", "游", "郏", "竺", "权", "逯", "盖", "益", "桓", "公", "仉",
                "督", "岳", "帅", "缑", "亢", "况", "郈", "有", "琴", "归", "海", "晋", "楚", "闫", "法", "汝", "鄢", "涂", "钦", "商", "牟", "佘", "佴", "伯", "赏", "墨",
                "哈", "年", "爱", "阳", "佟", "言", "福", "南", "火", "铁", "迟", "漆", "官", "冼", "真", "展", "繁", "檀", "祭", "密", "敬", "揭", "舜", "楼",
                "疏", "冒", "浑", "挚", "胶", "随", "高", "皋", "原", "种", "练", "弥", "仓", "眭", "蹇", "覃", "阿", "门", "恽", "来", "綦", "召", "仪", "风", "介", "巨",
                "木", "京", "狐", "郇", "虎", "枚", "抗", "达", "杞", "苌", "折", "麦", "庆", "过", "竹", "端", "鲜", "皇", "亓", "老", "是", "秘", "畅", "邝", "还", "宾",
                "闾", "辜", "纵", "侴", "司马", "上官", "欧阳", "夏侯", "诸葛", "东方"};
        for (int i = 0; i < n; i++) {
            String str = null;
            //定义高低位
            int hightPos, lowPos;
            Random random2 = new Random();
            hightPos = (176 + Math.abs(random2.nextInt(39)));//获取高位
            lowPos = (161 + Math.abs(random2.nextInt(93)));//获取低位
            byte[] b = new byte[2];
            b[0] = (new Integer(hightPos).byteValue());
            b[1] = (new Integer(lowPos).byteValue());
            try {
                str = new String(b, "GBK");//转成中文
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            ming += str;//随机名
        }
        int index = random1.nextInt(xing.length);
        String x = xing[index];//随机姓
        return x + ming;
    }

    /**
     * 获得随机护照号
     * @return
     */
    public static String getRandomPassport() {
        String num = "G";
        for (int i = 0; i < 8; i++) {
            //每次循环都从0~9挑选一个随机数
            num += (int) (Math.random() * 10);
        }
        return num;
    }
}
